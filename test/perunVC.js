import config, {alice, bob, ingrid, custodian} from './config';
import Party from './party';
import Contract from './contract';
import {
  mine,
  test,
  newTest,
  shouldThrow,
  expectBnEqual,
  VCState,
  expectBalancesAfterTransaction,
  verifyEVCAlreadyClosed,
  AppointmentType,
} from './testUtils';
import chai from 'chai';

const expect = chai.expect;
const Perun = artifacts.require('Perun');

let Alice;
let Bob;
let IngridAlice;
let IngridBob;
let CAlice;
let CBob;
const {aliceBalance, ingridAliceBalance} = config;
const newAliceBalance = aliceBalance + 2;
const newIngridAliceBalance = ingridAliceBalance - 2;
const newVersion = 3;
const aliceOfflineDelta = 10;
const delta = 5;
const vcId = 7;
const vcAliceBalance = 3;
const vcBobBalance = 4;
const vcValidity = 10;
const vcVersion = 3;
const ocData = {
  id: vcId,
  balance1: vcAliceBalance,
  balance2: vcBobBalance,
  party1: alice,
  party2: bob,
  ingrid: ingrid,
  validity: vcValidity,
  subchannel1: 'filled by setUp',
  subchannel2: 'filled by setUp',
};
let ocAlice;
let ocBob;
let ocIngrid;
const lcUpdate = {
  id: 'filled by setUp',
  balance1: newAliceBalance,
  balance2: newIngridAliceBalance,
  version: newVersion,
};
const vcUpdate = {
  lcId: 'filled by setUp',
  vcId: vcId,
  balance1: vcAliceBalance + 1,
  balance2: vcBobBalance - 1,
  version: newVersion,
};
const cc = {
  vcId,
  lcUpdate,
};
const nonce = 44;
const ca = {
  lcId: 'filled by setUp',
  vcId: vcId,
  version: 5,
  hash: 'filled by setUp',
  issuer: alice,
  appointmentType: AppointmentType.VCState,
};
let lcAlice;
let lcBob;
let lcIngridAlice;
let vcUpdateAlice;
let vcUpdateBob;
let ccAlice;
let ccIngridAlice;
let caAlice;

export const setUp = async (accounts) => {
  const instance =
    await Perun.new(ingrid, {from: alice, value: config.aliceBalance});
  const instance2 =
    await Perun.new(ingrid, {from: bob, value: config.bobBalance});
  Alice =
    new Party(alice, config.aliceBalance, config.parties[0].key, instance);
  Bob = new Party(bob, config.bobBalance, config.parties[1].key, instance2);
  IngridAlice = new Party(ingrid, config.ingridAliceBalance, config.parties[2].key, instance);
  IngridBob = new Party(ingrid, config.ingridBobBalance, config.parties[2].key, instance2);
  CAlice = new Contract(instance);
  CBob = new Contract(instance2);
  ocData.subchannel1 = instance.address;
  ocData.subchannel2 = instance2.address;
  ocAlice = await Alice.signOC(ocData);
  ocBob = await Bob.signOC(ocData);
  ocIngrid = await IngridAlice.signOC(ocData);
  lcUpdate.id = instance.address;
  lcAlice = await Alice.signLCUpdate(lcUpdate);
  lcBob = await Bob.signLCUpdate(lcUpdate);
  lcIngridAlice = await IngridAlice.signLCUpdate(lcUpdate);
  vcUpdate.lcId = instance.address;
  vcUpdateAlice = await Alice.signVCUpdate(vcUpdate);
  vcUpdateBob = await Bob.signVCUpdate(vcUpdate);
  ccAlice = await Alice.signCC(cc);
  ccIngridAlice = await IngridAlice.signCC(cc);
  ca.lcId = instance.address;
  ca.hash = web3.utils.soliditySha3(vcUpdate.lcId, vcUpdate.vcId, vcUpdate.balance1, vcUpdate.balance2, vcUpdate.version, nonce);
  caAlice = await Alice.signCA(ca);
  await mine(30);
};

test('Perun', 'Ingrid should be allowed to close the channel if Alice doesn\'t provide VC state', setUp, async () => {
  await IngridAlice.openConfirm();

  await IngridAlice.requestAliceVCState(ocData, ocBob);

  expect(await CAlice.getVcAlice(vcId)).to.equal(alice);
  expect(await CAlice.getVcBob(vcId)).to.equal(bob);
  expectBnEqual(await CAlice.getVcAliceBalance(vcId), vcAliceBalance);
  expectBnEqual(await CAlice.getVcBobBalance(vcId), vcBobBalance);
  expectBnEqual(await CAlice.getVcValidity(vcId), vcValidity);
  expectBnEqual(await CAlice.getVcState(vcId), VCState.AliceVCStateRequested);
  expectBnEqual(await CAlice.getVcVersion(vcId), 0);

  const events = await CAlice.getEvents('AliceVCStateRequested');
  expect(events).to.have.length(1);
  expectBnEqual(events[0].args[0], vcId);

  await shouldThrow(IngridAlice.aliceDidntProvideRequestedVCState(vcId));
  await mine(delta);
  await shouldThrow(IngridAlice.aliceDidntProvideRequestedVCState(vcId + 1));
  await IngridAlice.aliceDidntProvideRequestedVCState(vcId);

  expect(await CAlice.isClosed()).to.be.true;
});

test('Perun', 'Ingrid should receive CC from VCAlreadyClosed if Alice cooperates', setUp, async () => {
  await IngridAlice.openConfirm(); 
  await IngridAlice.requestAliceVCState(ocData, ocBob);

  await Alice.vcAlreadyClosed(cc, ccAlice, ccIngridAlice, lcAlice, lcIngridAlice);
  expectBnEqual(await CAlice.getVcState(vcId), VCState.Closed);
  
  await verifyEVCAlreadyClosed(CAlice, vcId, lcUpdate, ccAlice, ccIngridAlice, lcAlice, lcIngridAlice);

  expect(await CAlice.isClosed()).to.be.false;
});

test('Perun', 'Alice should be allowed to provide VC state before timeout', setUp, async () => {
  await IngridAlice.openConfirm();
  await IngridAlice.requestAliceVCState(ocData, ocBob);
  await Alice.provideVCState(vcUpdate, vcUpdateAlice, vcUpdateBob);

  expectBnEqual(await CAlice.getVcAliceBalance(vcId), vcUpdate.balance1);
  expectBnEqual(await CAlice.getVcBobBalance(vcId), vcUpdate.balance2);
  expectBnEqual(await CAlice.getVcVersion(vcId), vcUpdate.version);
  expectBnEqual(await CAlice.getVcState(vcId), VCState.AliceVCStateProvided);
  expectBnEqual(await CAlice.getVcBlockTimeout(vcId), 0);

  const events = await CAlice.getEvents('AliceVCStateProvided');
  expect(events).to.have.length(1);
  const e = events[0];
  expect(e.args.vcUpdate.lcId).to.equal(vcUpdate.lcId + '');
  expect(e.args.vcUpdate.vcId).to.equal(vcUpdate.vcId + '');
  expect(e.args.vcUpdate.balance1).to.equal(vcUpdate.balance1 + '');
  expect(e.args.vcUpdate.balance2).to.equal(vcUpdate.balance2 + '');
  expect(e.args.vcUpdate.version).to.equal(vcUpdate.version + '');
  expect(e.args.sigA).to.equal(vcUpdateAlice);
  expect(e.args.sigB).to.equal(vcUpdateBob);

  expect(await CAlice.isClosed()).to.be.false;
});

test('Perun', 'Alice should be able to claim that Ingrid didn\'t close the channel and claim the reward', setUp, async () => {
  await IngridAlice.openConfirm();
  // No need to mine() because ocData.validity is low.
  await Alice.claimThatIngridDidntCloseVCOnTime(ocData, ocBob);
  expectBnEqual(await CAlice.getVcState(vcId), VCState.AliceSaysIngridDidntCloseVCOnTime);
  const events = await CAlice.getEvents('AliceSaysIngridDidntCloseVCOnTime');
  expect(events).to.have.length(1);
  expectBnEqual(events[0].args.vcId, vcId);

  await shouldThrow(Alice.ingridDidntCloseVCOnTime(vcId));
  await mine(delta);
  await Alice.ingridDidntCloseVCOnTime(vcId);

  expect(await CAlice.getEvents('LCClosed')).to.have.length(1);

  expect(await CAlice.isClosed()).to.be.true;
});

test('Perun', 'Ingrid should be able to prove she\'s closed the channel if Alice challenges her', setUp, async () => {
  await IngridAlice.openConfirm();
  await Alice.claimThatIngridDidntCloseVCOnTime(ocData, ocBob);
  await mine(delta);

  await shouldThrow(Alice.vcAlreadyClosed(cc, ccAlice, ccIngridAlice, lcAlice, lcIngridAlice));
  await IngridAlice.vcAlreadyClosed(cc, ccAlice, ccIngridAlice, lcAlice, lcIngridAlice);
  expectBnEqual(await CAlice.getVcState(vcId), VCState.Closed);

  await verifyEVCAlreadyClosed(CAlice, vcId, lcUpdate, ccAlice, ccIngridAlice, lcAlice, lcIngridAlice);
  
  expect(await CAlice.isClosed()).to.be.false;
});

test('Perun', 'Ingrid should be allowed to set newer VC state', setUp, async () => {
  const vcUpdate2 = {
    ...vcUpdate,
    balance1: vcUpdate.balance1 - 1,
    balance2: vcUpdate.balance2 + 1,
    version: vcUpdate.version + 1,
  };
  const vcUpdate2Alice = await Alice.signVCUpdate(vcUpdate2);
  const vcUpdate2Bob = await Bob.signVCUpdate(vcUpdate2);

  await IngridAlice.openConfirm();
  await shouldThrow(IngridAlice.closeVC(ocData, ocAlice, ocBob, vcUpdate2, vcUpdate2Alice, vcUpdate2Bob));
  await IngridAlice.requestAliceVCState(ocData, ocBob);
  await shouldThrow(IngridAlice.closeVC(ocData, ocAlice, ocBob, vcUpdate2, vcUpdate2Alice, vcUpdate2Bob));
  await Alice.provideVCState(vcUpdate, vcUpdateAlice, vcUpdateBob);

  await IngridAlice.closeVC(ocData, ocAlice, ocBob, vcUpdate2, vcUpdate2Alice, vcUpdate2Bob);

  const events = await CAlice.getEvents('IngridClosingVC');
  expect(events).to.have.length(1);
  expectBnEqual(events[0].args.vcId, vcId);

  expectBnEqual(await CAlice.getVcState(vcId), VCState.IngridProvidedVCState);
  expectBnEqual(await CAlice.getVcAliceOpeningBalance(vcId), ocData.balance1);
  expectBnEqual(await CAlice.getVcBobOpeningBalance(vcId), ocData.balance2);
  expectBnEqual(await CAlice.getVcVersion(vcId), vcUpdate2.version);
  expectBnEqual(await CAlice.getVcAliceBalance(vcId), vcUpdate2.balance1);
  expectBnEqual(await CAlice.getVcBobBalance(vcId), vcUpdate2.balance2);

  expect(await CAlice.isClosed()).to.be.false;
});

test('Perun', 'Alice\'s state shoud be canonical if Ingrid can\'t provide newer one', setUp, async () => {
  const vcUpdate2 = {
    ...vcUpdate,
    balance1: vcUpdate.balance1 - 1,
    balance2: vcUpdate.balance2 + 1,
    version: vcUpdate.version - 1,
  };
  const vcUpdate2Alice = await Alice.signVCUpdate(vcUpdate2);
  const vcUpdate2Bob = await Bob.signVCUpdate(vcUpdate2);

  await IngridAlice.openConfirm();
  await shouldThrow(IngridAlice.closeVC(ocData, ocAlice, ocBob, vcUpdate2, vcUpdate2Alice, vcUpdate2Bob));
  await IngridAlice.requestAliceVCState(ocData, ocBob);
  await shouldThrow(IngridAlice.closeVC(ocData, ocAlice, ocBob, vcUpdate2, vcUpdate2Alice, vcUpdate2Bob));
  await Alice.provideVCState(vcUpdate, vcUpdateAlice, vcUpdateBob);

  await IngridAlice.closeVC(ocData, ocAlice, ocBob, vcUpdate2, vcUpdate2Alice, vcUpdate2Bob);

  const events = await CAlice.getEvents('IngridClosingVC');
  expect(events).to.have.length(1);
  expectBnEqual(events[0].args.vcId, vcId);

  expectBnEqual(await CAlice.getVcState(vcId), VCState.IngridProvidedVCState);
  expectBnEqual(await CAlice.getVcAliceOpeningBalance(vcId), ocData.balance1);
  expectBnEqual(await CAlice.getVcBobOpeningBalance(vcId), ocData.balance2);
  expectBnEqual(await CAlice.getVcVersion(vcId), vcUpdate.version);
  expectBnEqual(await CAlice.getVcAliceBalance(vcId), vcUpdate.balance1);
  expectBnEqual(await CAlice.getVcBobBalance(vcId), vcUpdate.balance2);

  expect(await CAlice.isClosed()).to.be.false;
});

test('Perun', 'Alice\'s internal balance should be updated in case of on-chain closing', setUp, async () => {
  await IngridAlice.openConfirm();
  await IngridAlice.requestAliceVCState(ocData, ocBob);
  await Alice.provideVCState(vcUpdate, vcUpdateAlice, vcUpdateBob);
  await IngridAlice.closeVC(ocData, ocAlice, ocBob, vcUpdate, vcUpdateAlice, vcUpdateBob);
  await shouldThrow(IngridAlice.closeVCTimeout(vcId));
  await mine(delta);
  await IngridAlice.closeVCTimeout(vcId);

  expectBnEqual(await CAlice.getVcAliceSum(), 1);
  expectBnEqual(await CAlice.getVcBobSum(), -1);
  expectBnEqual(await CAlice.getVcState(vcId), VCState.Closed);

  const events = await CAlice.getEvents('VCClosed');
  expect(events).to.have.length(1);
  expectBnEqual(events[0].args.vcId, vcId);

  expect(await CAlice.isClosed()).to.be.false;
});

test('Perun', 'Alice should be allowed to interrupt on-chain closing by providing a closing certificate', setUp, async () => {
  await IngridAlice.openConfirm();
  await IngridAlice.requestAliceVCState(ocData, ocBob);
  await Alice.provideVCState(vcUpdate, vcUpdateAlice, vcUpdateBob);
  await IngridAlice.closeVC(ocData, ocAlice, ocBob, vcUpdate, vcUpdateAlice, vcUpdateBob);
  await mine(delta);
  await Alice.vcAlreadyClosed(cc, ccAlice, ccIngridAlice, lcAlice, lcIngridAlice);
  await shouldThrow(IngridAlice.closeVCTimeout(vcId));
  expectBnEqual(await CAlice.getVcState(vcId), VCState.Closed);

  await verifyEVCAlreadyClosed(CAlice, vcId, lcUpdate, ccAlice, ccIngridAlice, lcAlice, lcIngridAlice);

  expectBnEqual(await CAlice.getVcAliceSum(), 0);
  expectBnEqual(await CAlice.getVcBobSum(), 0);
  expectBnEqual(await CAlice.getVcState(vcId), VCState.Closed);

  expect(await CAlice.isClosed()).to.be.false;
});

test('Perun', 'Alice should be able to interrupt LC closing by providing oc for an open VC', setUp, async () => {
  await IngridAlice.openConfirm();
  await IngridAlice.close(lcUpdate, lcAlice);

  await shouldThrow(IngridAlice.vcActive(ocData, ocAlice));
  await Alice.vcActive(ocData, ocIngrid);

  expect(await CAlice.isClosed()).to.be.true;
});

test('Perun', 'Ingrid should be able to interrupt LC closing by providing oc for an open VC', setUp, async () => {
  await IngridAlice.openConfirm();
  await Alice.close(lcUpdate, lcIngridAlice);

  await shouldThrow(Alice.vcActive(ocData, ocIngrid));
  await IngridAlice.vcActive(ocData, ocAlice);

  expect(await CAlice.isClosed()).to.be.true;
});

test('Perun', 'contract should accept VC hashes and update them if version is newer', setUp, async () => {
  await IngridAlice.openConfirm();

  const ca2 = {
    ...ca,
    version: 3,
  };

  const sig = await Alice.signCA(ca2);
  await shouldThrow(CAlice.provideVCStateHash(ca, caAlice, custodian));

  const now = await web3.eth.getBlockNumber();
  await IngridAlice.requestAliceVCState(ocData, ocBob);
  expectBnEqual(await CAlice.getVCDisputeStart(vcId), now + 1);

  await CAlice.provideVCStateHash(ca, caAlice, custodian);

  const events = await CAlice.getEvents('VCHashProvidedByCustodian');
  expect(events).to.have.length(1);
  expect(events[0].args[0]).to.equal(ca2.hash);
  expectBnEqual(events[0].args[1], vcId);


  const provided = await CAlice.getProvidedByCustodianInDispute(vcId);
  expect(provided).to.have.length(1);
  expectBnEqual(provided[0], ca.version);

  expectBnEqual(await CAlice.getVcCustodianVersion(vcId), 5);
  expect(await CAlice.getVCCustodianHash(vcId)).to.equal(ca.hash);

  await CAlice.provideVCStateHash(ca2, sig, custodian);

  expectBnEqual(await CAlice.getVcCustodianVersion(vcId), 5);
  expect(await CAlice.getVCCustodianHash(vcId)).to.equal(ca.hash);
});

test('Perun', 'Ingrid should have to wait a bit longer if custodian provides Alice\'s state hash', setUp, async () => {
  await IngridAlice.openConfirm(); 
  await IngridAlice.requestAliceVCState(ocData, ocBob);
  await CAlice.provideVCStateHash(ca, caAlice, custodian);
  await mine(delta);
  await shouldThrow(IngridAlice.aliceDidntProvideRequestedVCState(vcId));
  await mine(aliceOfflineDelta - delta);
  await IngridAlice.aliceDidntProvideRequestedVCState(vcId);
  expect(await CAlice.isClosed()).to.be.true;
});

test('Perun', 'Alice should be allowed to provide blinding nonce within time window', setUp, async () => {
  await IngridAlice.openConfirm();
  await IngridAlice.requestAliceVCState(ocData, ocBob);
  await CAlice.provideVCStateHash(ca, caAlice, custodian);

  await shouldThrow(Alice.revealBlindingNonce(vcUpdate, nonce - 1, vcUpdateAlice, vcUpdateBob));
  await shouldThrow(Alice.revealBlindingNonce(vcUpdate, nonce, vcUpdateBob, vcUpdateBob));
  await shouldThrow(Alice.revealBlindingNonce(vcUpdate, nonce, vcUpdateAlice, vcUpdateAlice));

  await Alice.revealBlindingNonce(vcUpdate, nonce, vcUpdateAlice, vcUpdateBob);
});

test('Perun', 'Ingrid should have to wait a bit longer if custodian provides Alice\'s LC state hash', setUp, async () => {
  await IngridAlice.openConfirm();
  let sig = await Alice.signLCUpdate(lcUpdate);
  await IngridAlice.close(lcUpdate, sig);
  const newCa = {
    ...ca,
    appointmentType: AppointmentType.LCState,
  };
  sig = await Alice.signCA(newCa);
  await CAlice.closeByCustodian(custodian, newCa, sig, alice);
  const events = await CAlice.getEvents('LCCloseProvidedByCustodian');
  expect(events).to.have.length(1);
  expect(events[0].args[0]).to.equal(newCa.hash);

  await mine(delta);
  await shouldThrow(IngridAlice.closeTimeout());
  await mine(aliceOfflineDelta - delta);
  await IngridAlice.closeTimeout();
  expect(await CAlice.isClosed()).to.be.true;
});

test('Perun', 'Alice should be allowed to provide blinding nonce to LC within time window', setUp, async () => {
  await IngridAlice.openConfirm();
  let sig = await Alice.signLCUpdate(lcUpdate);
  await IngridAlice.close(lcUpdate, sig);
  const newCa = {
    ...ca,
    hash: web3.utils.soliditySha3(lcUpdate.id, lcUpdate.balance1, lcUpdate.balance2, lcUpdate.version, nonce),
    appointmentType: AppointmentType.LCState,
  };
  sig = await Alice.signCA(newCa);
  await CAlice.closeByCustodian(custodian, newCa, sig, alice);


  await shouldThrow(Alice.revealLCBlindingNonce(lcUpdate, nonce - 1, lcAlice, lcIngridAlice));
  await shouldThrow(Alice.revealLCBlindingNonce(lcUpdate, nonce, lcIngridAlice, lcIngridAlice));
  await shouldThrow(Alice.revealLCBlindingNonce(lcUpdate, nonce, lcAlice, lcAlice));
  await Alice.revealLCBlindingNonce(lcUpdate, nonce, lcAlice, lcIngridAlice);
});

test('Perun', 'Ingrid should have to wait a bit longer if custodian provides Alice\'s VCAlreadyClosed hash', setUp, async () => {
  await IngridAlice.openConfirm();
  await IngridAlice.requestAliceVCState(ocData, ocBob);
  const newCa = {
    ...ca,
    appointmentType: AppointmentType.VCAlreadyClosed,
    version: 0,
  };
  const sig = await Alice.signCA(newCa);
  await CAlice.vcAlreadyClosedCustodian(custodian, newCa, sig);
  const events = await CAlice.getEvents('VCAlreadyClosedProvidedByCustodian');
  expect(events).to.have.length(1);
  expect(events[0].args[0]).to.equal(newCa.hash);
  expectBnEqual(events[0].args[1], vcId);
  await mine(delta);
  await shouldThrow(IngridAlice.aliceDidntProvideRequestedVCState(vcId));
  await mine(aliceOfflineDelta - delta);
  await IngridAlice.aliceDidntProvideRequestedVCState(vcId);
  expect(await CAlice.isClosed()).to.be.true;
});

test('Perun', 'Alice should be allowed to provide blinding nonce to VCAlreadyClosed within time window', setUp, async () => {
  await IngridAlice.openConfirm();
  await IngridAlice.requestAliceVCState(ocData, ocBob);
  const newCa = {
    ...ca,
    hash: web3.utils.soliditySha3(cc.vcId, cc.lcUpdate.id, cc.lcUpdate.balance1, cc.lcUpdate.balance2, cc.lcUpdate.version, nonce),
    appointmentType: AppointmentType.VCAlreadyClosed,
    version: 0,
  };
  let sig = await Alice.signCA(newCa);
  await CAlice.vcAlreadyClosedCustodian(custodian, newCa, sig);

  await shouldThrow(Alice.revealAlreadyClosedBlindingNonce(cc, nonce - 1, ccAlice, ccIngridAlice, lcAlice, lcIngridAlice));
  await shouldThrow(Alice.revealAlreadyClosedBlindingNonce(cc, nonce, ccAlice, ccIngridAlice, lcAlice, lcAlice));
  await shouldThrow(Alice.revealAlreadyClosedBlindingNonce(cc, nonce, ccAlice, ccAlice, lcAlice, lcIngridAlice));
  await Alice.revealAlreadyClosedBlindingNonce(cc, nonce, ccAlice, ccIngridAlice, lcAlice, lcIngridAlice);
  expectBnEqual(await CAlice.getVcState(vcId), VCState.Closed);
  await verifyEVCAlreadyClosed(CAlice, vcId, lcUpdate, ccAlice, ccIngridAlice, lcAlice, lcIngridAlice);
});
