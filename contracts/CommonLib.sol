pragma solidity ^0.5.3;
pragma experimental ABIEncoderV2;

/** Common utils shared between Perun & Pisa. */
library CommonLib {
  uint constant vCStateAppointmentType = 0;
  uint constant lCStateAppointmentType = 1;
  uint constant vCClosedAppointmentType = 2;

  // Solidity doesn't allow directly accessing public constants from contracts.
  function VCStateAppointmentType() public pure returns(uint) {
    return vCStateAppointmentType;
  }

  function LCStateAppointmentType() public pure returns(uint) {
    return lCStateAppointmentType;
  }

  function VCClosedAppointmentType() public pure returns(uint) {
    return vCClosedAppointmentType;
  }

  /** Get the address that signed the given message. */
  function recover(bytes32 hash, bytes memory sig) public pure returns (address) {
    hash = getHash(hash);
    bytes32 r;
    bytes32 s;
    uint8 v;

    if (sig.length != 65) {
      return (address(0));
    }
    assembly {
      r := mload(add(sig, 32))
      s := mload(add(sig, 64))
      v := byte(0, mload(add(sig, 96)))
    }
    if (v < 27) {
      v += 27;
    }
    if (v != 27 && v != 28) {
      return (address(0));
    } else {
      return ecrecover(hash, v, r, s);
    }
  }

  /** Get hash of the message to sign. */
  function getHash(bytes32 m) private pure returns (bytes32) {
    return keccak256(abi.encodePacked("\x19Ethereum Signed Message:\n32", m));
  }
}
