import {State} from './testUtils';

export default class Contract {
  constructor(instance) {
    this.instance = instance;
  }

  async getAlice() {
    const address = await this.instance.alice.call();
    return address.toLowerCase();
  }

  async getBob() {
    const address = await this.instance.bob.call();
    return address.toLowerCase();
  }

  async getEvents(name) {
    return await this.instance.getPastEvents(
        name, {fromBlock: 0, toBlock: 'latest'});
  }

  async getCurrentState() {
    return await this.instance.currentState.call();
  }

  async getAliceBalance() {
    return await this.instance.aliceBalance.call();
  }

  async getBobBalance() {
    return await this.instance.bobBalance.call();
  }

  async getAliceClosing() {
    return await this.instance.aliceClosing.call();
  }

  async isClosed() {
    const currentState = await this.getCurrentState();
    return currentState.eq(State.Closed);
  }

  async getVcAlice(vcId) {
    const vc = await this.instance.vcs.call(vcId);
    return vc.party1.toLowerCase();
  }

  async getVcBob(vcId) {
    const vc = await this.instance.vcs.call(vcId);
    return vc.party2.toLowerCase();
  }

  async getVcAliceBalance(vcId) {
    const vc = await this.instance.vcs.call(vcId);
    return vc.balance1;
  }

  async getVcBobBalance(vcId) {
    const vc = await this.instance.vcs.call(vcId);
    return vc.balance2;
  }

  async getVcValidity(vcId) {
    const vc = await this.instance.vcs.call(vcId);
    return vc.validity;
  }

  async getVcState(vcId) {
    const vc = await this.instance.vcs.call(vcId);
    return vc.state;
  }

  async getVcVersion(vcId) {
    const vc = await this.instance.vcs.call(vcId);
    return vc.version;
  }

  async getVcBlockTimeout(vcId) {
    const vc = await this.instance.vcs.call(vcId);
    return vc.blockTimeout;
  }

  async getVcAliceOpeningBalance(vcId) {
    const vc = await this.instance.vcs.call(vcId);
    return vc.openingBalance1;
  }

  async getVcBobOpeningBalance(vcId) {
    const vc = await this.instance.vcs.call(vcId);
    return vc.openingBalance2;
  }

  async getVcAliceSum() {
    return await this.instance.vcAliceSum.call();
  }

  async getVcBobSum() {
    return await this.instance.vcBobSum.call();
  }


  async provideVCStateHash(cu, sig, from) {
    return await this.instance.provideVCStateHash(cu, sig, {from});
  }

  async getVcCustodianVersion(vcId) {
    const c = await this.instance.vcCustodianData.call(vcId);
    return c.stateHashVersion;
  }

  async getVCCustodianHash(vcId) {
    const c = await this.instance.vcCustodianData.call(vcId);
    return c.stateHash;
  }

  async getVCDisputeStart(vcId) {
    const c = await this.instance.vcDisputes.call(vcId);
    return c.startTime;
  }

  async getProvidedByCustodianInDispute(vcId) {
    const arr = [];
    const length = await this.instance.getProvidedByCustodianLength.call(vcId);
    for (let i = 0; i < length; i++) {
      arr.push(await this.instance.getProvidedByCustodianAt(vcId, i));
    }
    return arr;
  }

  async closeByCustodian(custodian, ...args) {
    return await this.instance.closeByCustodian(...args, {from: custodian});
  }

  async vcAlreadyClosedCustodian(custodian, ...args) {
    return await this.instance.VCAlreadyClosedCustodian(...args, {from: custodian});
  }
}
