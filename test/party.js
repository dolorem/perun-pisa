const BN = web3.utils.BN;

const assertNotNull = (value) => {
  if (value == null || value == undefined) {
    throw new Error("Undefined arg");
  }
};

export default class Party {
  constructor(address, balance, key, contract) {
    this.address = address;
    this.balance = balance;
    this.key = key;
    this.contract = contract;
  }

  async sign(...args) {
    const hash = web3.utils.soliditySha3(...args);
    const signature = await web3.eth.accounts.sign(hash, this.key);
    return signature.signature;
  }

  async openTimeout() {
    return await this.contract.openTimeout.sendTransaction({from: this.address});
  }

  async openConfirm() {
    return await this.contract.openConfirm.sendTransaction(
        {from: this.address, value: this.balance});
  }

  async close(lcUpdate, sig) {
    return await this.contract.close.sendTransaction(
        lcUpdate, sig, {from: this.address});
  }

  async claimThatIngridDidntCloseVCOnTime(...args) {
    return await this.contract.claimThatIngridDidntCloseVCOnTime.sendTransaction(
        ...args, {from: this.address});
  }

  async getBalance() {
    const balance = await web3.eth.getBalance(this.address);
    return new BN(balance);
  }

  async requestAliceVCState(ocData, ocSig) {
    return await this.contract.requestAliceVCState.sendTransaction(ocData, ocSig, {from: this.address});
  }

  async aliceDidntProvideRequestedVCState(vcId) {
    return await this.contract.aliceDidntProvideRequestedVCState.sendTransaction(vcId, {from: this.address});
  }

  async provideVCState(...args) {
    return await this.contract.provideVCState.sendTransaction(...args, {from: this.address});
  }

  async vcAlreadyClosed(...args) {
    return await this.contract.VCAlreadyClosed(...args, {from: this.address});
  }

  async ingridDidntCloseVCOnTime(vcId) {
    return await this.contract.ingridDidntCloseVCOnTime.sendTransaction(vcId, {from: this.address});
  }

  async signOC(oc) {
    return await this.sign(oc.id, oc.balance1, oc.balance2, oc.party1, oc.party2, oc.ingrid, oc.validity, oc.subchannel1, oc.subchannel2);
  }

  async signLCUpdate(lcUpdate) {
    return await this.sign(lcUpdate.id, lcUpdate.balance1, lcUpdate.balance2, lcUpdate.version);
  }

  async signVCUpdate(vcUpdate) {
    return await this.sign(vcUpdate.lcId, vcUpdate.vcId, vcUpdate.balance1, vcUpdate.balance2, vcUpdate.version);
  }

  async signCA(ca) {
    return await this.sign(ca.lcId, ca.vcId, ca.version, ca.hash, ca.appointmentType);
  }

  async signCC(cc) {
    return await this.sign(cc.vcId, cc.lcUpdate.id, cc.lcUpdate.balance1, cc.lcUpdate.balance2, cc.lcUpdate.version);
  }

  async closeVC(...args) {
    return await this.contract.closeVC.sendTransaction(...args, {from: this.address});
  }

  async signVCBalance(id, aliceBalance, bobBalance, version, ...other) {
    if (other.length != 0) {
      throw new Error('Invalid number of arguments passed to signVCBalance.');
    }
    assertNotNull(id);
    assertNotNull(aliceBalance);
    assertNotNull(bobBalance);
    assertNotNull(version);
  
    return await this.sign(id, aliceBalance, bobBalance, version);
  }

  async closeVCTimeout(...args) {
    return await this.contract.closeVCTimeout.sendTransaction(...args, {from: this.address});
  }

  async vcActive(oc, sig) {
    return await this.contract.VCActive.sendTransaction(oc, sig, {from: this.address});
  }

  async closeTimeout() {
    return await this.contract.closeTimeout.sendTransaction({from: this.address});
  }

  async revealBlindingNonce(...args) {
    return await this.contract.revealBlindingNonce.sendTransaction(...args, {from: this.address});
  }

  async closeByCustodian(...args) {
    return await this.contract.closeByCustodian(...args, {from: this.address});
  }

  async revealLCBlindingNonce(...args) {
    return await this.contract.revealLCBlindingNonce(...args, {from: this.address});
  }

  async revealAlreadyClosedBlindingNonce (...args) {
    return await this.contract.revealAlreadyClosedBlindingNonce(...args, {from: this.address});
  }
}
