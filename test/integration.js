import config, {alice, bob, ingrid, custodian} from './config';
import PisaParty from './partyPisa';
import PisaContract from './contractPisa';
import Party from './party';
import Contract from './contract';
import {
  mine,
  test,
  newTest,
  shouldThrow,
  expectBnEqual,
  VCState,
  expectBalancesAfterTransaction,
  verifyEVCAlreadyClosed,
  CustodianState,
  AppointmentType,
} from './testUtils';
import chai from 'chai';

const expect = chai.expect;
const Perun = artifacts.require('Perun');
const Pisa = artifacts.require('Pisa');

const tSettle = 5;
const tWithdraw = 5;
const custodianDeposit = 1000;
const delta = 5;
const vcId = 70;
const vcAliceBalance = 7;
const vcBobBalance = 3;
const vcValidity = 1;
const vcVersion = 5;
const nonce = 44;
const ocData = {
  id: vcId,
  balance1: vcAliceBalance,
  balance2: vcBobBalance,
  party1: alice,
  party2: bob,
  ingrid: ingrid,
  validity: vcValidity,
  subchannel1: 'filled by setUp',
  subchannel2: 'filled by setUp',
};
const receipt = {
  stateChannel: 'filled by setUp',
  virtualChannel: vcId,
  tStart: 1,
  tEnd: 2,
  nonce,
  party: alice,
  version: vcVersion,
  appointmentType: AppointmentType.VCState,
};
const ca = {
  lcId: 'filled by setUp',
  vcId,
  version: vcVersion,
  hash: web3.utils.soliditySha3(vcId, vcAliceBalance + 1, vcBobBalance - 1, vcVersion, nonce),
  issuer: alice,
  appointmentType: AppointmentType.VCState,
};
const vcUpdate = {
  lcId: 'filled by setUp',
  vcId: vcId,
  version: vcVersion,
  balance1: vcAliceBalance - 1,
  balance2: vcBobBalance + 1,
};
const lcUpdate = {
  id: 'filled by setUp',
  balance1: config.aliceBalance - 1,
  balance2: config.ingridAliceBalance + 1,
  version: vcVersion,
};
const cc = {
  vcId,
  lcUpdate,
};
let CC;
let CAlice;
let Alice;
let AlicePisa;
let Ingrid;
let IngridPisa;
let Bob;
let Custodian;
let ocBob;

const setUp = async () => {
  const perun = await Perun.new(ingrid, {from: alice, value: config.aliceBalance});
  const perun2 = await Perun.new(ingrid, {from: bob, value: config.bobBalance});
  const pisa = await Pisa.new(tSettle, tWithdraw, {from: custodian, value: custodianDeposit});
  Alice = new Party(alice, config.aliceBalance, config.parties[0].key, perun);
  AlicePisa = new PisaParty(alice, config.parties[0].key, pisa);
  Bob = new Party(bob, config.bobBalance, config.parties[1].key, perun2);
  Ingrid = new Party(ingrid, config.ingridAliceBalance, config.parties[2].key, perun);
  IngridPisa = new PisaParty(ingrid, config.parties[2].key, pisa);
  CC = new PisaContract(pisa);
  CAlice = new Contract(perun);
  Custodian = new PisaParty(custodian, config.parties[3].key, pisa);
  ocData.subchannel1 = perun.address;
  ocData.subchannel2 = perun2.address;
  ocBob = await Bob.signOC(ocData);
  receipt.stateChannel = perun.address;
  lcUpdate.id = perun.address;
  vcUpdate.lcId = perun.address;
  ca.lcId = perun.address;
};

test('Pisa should be able to detect cheating Ingrid', setUp, async () => {
  await Ingrid.openConfirm(); 
  const blockNumber = await web3.eth.getBlockNumber();
  const newReceipt = {
    ...receipt,
    tStart: blockNumber,
    tEnd: blockNumber + 2,
  };
  const sig = await Custodian.signReceipt(newReceipt);
  await Ingrid.requestAliceVCState(ocData, ocBob);
  await mine(tSettle + 1);
  await AlicePisa.recourse(newReceipt, sig);
  expectBnEqual(await CC.getState(), CustodianState.Cheated);
});

test('Pisa should be able to detect honest Ingrid', setUp, async () => {
  await Ingrid.openConfirm(); 
  const blockNumber = await web3.eth.getBlockNumber();
  const newReceipt = {
    ...receipt,
    tStart: blockNumber,
    tEnd: blockNumber + 2,
  };
  await Ingrid.requestAliceVCState(ocData, ocBob);

  let sig = await Alice.signCA(ca);
  await CAlice.provideVCStateHash(ca, sig, custodian);

  await mine(tSettle + 1);
  sig = await Custodian.signReceipt(newReceipt);
  await AlicePisa.recourse(newReceipt, sig);
  expectBnEqual(await CC.getState(), CustodianState.Open);
});

test('Pisa should be able to detect if Custodian cheated on LC', setUp, async () => {
  await Ingrid.openConfirm();
  const blockNumber = await web3.eth.getBlockNumber();
  const newReceipt = {
    ...receipt,
    tStart: blockNumber,
    tEnd: blockNumber + 2,
    appointmentType: AppointmentType.LCState,
  };

  let sig = await Alice.signLCUpdate(lcUpdate);
  await Ingrid.close(lcUpdate, sig);
  
  sig = await Custodian.signReceipt(newReceipt);
  await mine(tSettle + 1);
  await AlicePisa.recourse(newReceipt, sig);
  expectBnEqual(await CC.getState(), CustodianState.Cheated);
});

test('Pisa should be able to detect honest Custodian on LC', setUp, async () => {
  await Ingrid.openConfirm();
  const blockNumber = await web3.eth.getBlockNumber();
  const newReceipt = {
    ...receipt,
    tStart: blockNumber,
    tEnd: blockNumber + 2,
    appointmentType: AppointmentType.LCState,
  };

  let sig = await Alice.signLCUpdate(lcUpdate);
  await Ingrid.close(lcUpdate, sig);
  
  const newCa = {
    ...ca,
    appointmentType: AppointmentType.LCState,
  };
  sig = await Alice.signCA(newCa);
  await Ingrid.closeByCustodian(newCa, sig, alice);

  sig = await Custodian.signReceipt(newReceipt);
  await mine(tSettle + 1);
  await AlicePisa.recourse(newReceipt, sig);
  expectBnEqual(await CC.getState(), CustodianState.Open);
});

test('Pisa should be able to detect if Custodian cheated on VCAlreadyClosed', setUp, async () => {
  await Ingrid.openConfirm();
  const blockNumber = await web3.eth.getBlockNumber();
  const newReceipt = {
    ...receipt,
    tStart: blockNumber,
    tEnd: blockNumber + 2,
    appointmentType: AppointmentType.VCAlreadyClosed,
  };

  let sig = await Alice.signLCUpdate(lcUpdate);
  await Ingrid.requestAliceVCState(ocData, ocBob);
  
  sig = await Custodian.signReceipt(newReceipt);
  await mine(tSettle + 1);
  await AlicePisa.recourse(newReceipt, sig);
  expectBnEqual(await CC.getState(), CustodianState.Cheated);
});

test('Pisa should be able to detect honest Custodian on VCAlreadyClosed', setUp, async () => {
  await Ingrid.openConfirm();
  const blockNumber = await web3.eth.getBlockNumber();
  const newReceipt = {
    ...receipt,
    tStart: blockNumber,
    tEnd: blockNumber + 2,
    appointmentType: AppointmentType.VCAlreadyClosed,
    version: 0,
  };

  let sig = await Alice.signLCUpdate(lcUpdate);
  await Ingrid.requestAliceVCState(ocData, ocBob);

  const newCa = {
    ...ca,
    appointmentType: AppointmentType.VCAlreadyClosed,
    version: 0,
  };
  sig = await Alice.signCA(newCa);
  await CAlice.vcAlreadyClosedCustodian(custodian, newCa, sig);
  
  sig = await Custodian.signReceipt(newReceipt);
  await mine(tSettle + 1);
  await AlicePisa.recourse(newReceipt, sig);
  expectBnEqual(await CC.getState(), CustodianState.Open);
});

test('Alice should be able to withdraw her deposit if Custodian cheated', setUp, async () => {
  await Ingrid.openConfirm(); 
  await AlicePisa.deposit(10);
  const blockNumber = await web3.eth.getBlockNumber();
  const newReceipt = {
    ...receipt,
    tStart: blockNumber,
    tEnd: blockNumber + 2,
  };
  const sig = await Custodian.signReceipt(newReceipt);
  await Ingrid.requestAliceVCState(ocData, ocBob);
  await mine(tSettle + 1);
  await AlicePisa.recourse(newReceipt, sig);
  expectBnEqual(await CC.getState(), CustodianState.Cheated);

  await AlicePisa.withdrawCheated();
  await shouldThrow(AlicePisa.withdrawCheated());
});

test('Ingrid shouldn\'t be punished if both parties close LC on-chain', setUp, async () => {
  await Ingrid.openConfirm();
  const blockNumber = await web3.eth.getBlockNumber();
  const newReceipt = {
    ...receipt,
    tStart: blockNumber,
    tEnd: blockNumber + 2,
    appointmentType: AppointmentType.LCState,
  };

  let sig = await Alice.signLCUpdate(lcUpdate);
  await Ingrid.close(lcUpdate, sig);
  sig = await Ingrid.signLCUpdate(lcUpdate);
  await Alice.close(lcUpdate, sig);
  
  sig = await Custodian.signReceipt(newReceipt);
  await mine(tSettle + 1);
  await AlicePisa.recourse(newReceipt, sig);
  expectBnEqual(await CC.getState(), CustodianState.Open);
});

test('Ingrid shouldn\'t be punished if Alice closes VC on-chain', setUp, async () => {
  await Ingrid.openConfirm();
  const blockNumber = await web3.eth.getBlockNumber();
  const newReceipt = {
    ...receipt,
    tStart: blockNumber,
    tEnd: blockNumber + 2,
    appointmentType: AppointmentType.VCAlreadyClosed,
    version: 0,
  };

  let sig = await Alice.signLCUpdate(lcUpdate);
  await Ingrid.requestAliceVCState(ocData, ocBob);
  let ccA = await Alice.signCC(cc);
  let ccI = await Ingrid.signCC(cc);
  let lcA = await Alice.signLCUpdate(lcUpdate);
  let lcI = await Ingrid.signLCUpdate(lcUpdate);
  await Alice.vcAlreadyClosed(cc, ccA, ccI, lcA, lcI);

  sig = await Custodian.signReceipt(newReceipt);
  await mine(tSettle + 1);
  await AlicePisa.recourse(newReceipt, sig);
  expectBnEqual(await CC.getState(), CustodianState.Open);
});

test('Ingrid shouldn\'t be punished if Alice provides VC state', setUp, async () => {
  await Ingrid.openConfirm(); 
  const blockNumber = await web3.eth.getBlockNumber();
  const newReceipt = {
    ...receipt,
    tStart: blockNumber,
    tEnd: blockNumber + 2,
  };
  await Ingrid.requestAliceVCState(ocData, ocBob);
  let sigA = await Alice.signVCUpdate(vcUpdate);
  let sigB = await Bob.signVCUpdate(vcUpdate);
  await Alice.provideVCState(vcUpdate, sigA, sigB);

  await mine(tSettle + 1);
  const sig = await Custodian.signReceipt(newReceipt);
  await AlicePisa.recourse(newReceipt, sig);
  expectBnEqual(await CC.getState(), CustodianState.Open);
});
