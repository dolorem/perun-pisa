pragma solidity ^0.5.3;

/** Library for performing safe arithmetic operations. Throws errors on overflow. */
library SafeMath {
  function add(uint x, uint y) internal pure returns (uint) {
    uint result = x + y;
    require(result >= x);
    return result;
  }

  function add(uint x, int y) internal pure returns (uint) {
    if (y >= 0) {
      return add(x, uint(y));
    }
    y *= - 1;
    return sub(x, uint(y));
  }

  function sub(uint x, uint y) internal pure returns (uint) {
    require(y <= x);
    return x - y;
  }

  function add(int x, int y) internal pure returns (int) {
    if (y >= 0) {
      int result = x + y;
      require(result >= x);
      return result;
    }
    y *= -1;
    return sub(x, y);
  }

  function sub(int x, int y) internal pure returns (int) {
    if (y >= 0) {
      int result = x - y;
      require(result <= x);
      return result;
    }
    y *= -1;
    return add(x, y);
  }
}
