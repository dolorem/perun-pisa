import config, {alice, bob, ingrid, custodian} from './config';
import Party from './partyPisa';
import Contract from './contractPisa';
import {
  mine,
  test,
  newTest,
  shouldThrow,
  expectBnEqual,
  VCState,
  expectBalanceAfterTransaction,
  expectBalancesAfterTransaction,
  verifyEVCAlreadyClosed,
  AppointmentType,
} from './testUtils';
import chai from 'chai';

const expect = chai.expect;
const Pisa = artifacts.require('Pisa');
const tSettle = 5;
const tWithdraw = 5;
const custodianDeposit = 1000;
const aliceBalance = 10;
const delta = 5;
let CC;
let Alice;
let Custodian;
const unratifiedReceipt = {
  stateChannel: custodian,  // any address will do
  virtualChannel: 2,
  tStart: 3,
  tEnd: 4,
  nonce: 0,
  party: alice,
  version: 7,
  appointmentType: AppointmentType.VCState,
};
const ratificationNonce = 4105411;
const ratifiedReceipt = {
  ...unratifiedReceipt,
  nonce: ratificationNonce,
};

const BN = web3.utils.BN;

const State = {
  Open: new BN(0),
  Cheated: new BN(1),
  Closing: new BN(2),
};

const ChannelState = {
  Default: new BN(0),
  Open: new BN(1),
  Closing: new BN(2),
  Closed: new BN(3),
};

const setUp = async (accounts) => {
  const instance = await Pisa.new(tSettle, tWithdraw, {from: custodian, value: custodianDeposit});
  Alice = new Party(alice, config.parties[0].key, instance);
  Custodian = new Party(custodian, config.parties[3].key, instance);
  CC = new Contract(instance);
};

test('Pisa', 'Alice should be able to deposit money once', setUp, async () => {
  expect(await CC.getCustodian()).to.equal(custodian);
  expectBnEqual(await CC.getCustodianDeposit(), custodianDeposit);
  expectBnEqual(await CC.getTSettle(), tSettle);
  expectBnEqual(await CC.getTWithdraw(), tWithdraw);
  expectBnEqual(await CC.getState(), State.Open);

  await Alice.deposit(aliceBalance);
  await shouldThrow(Alice.deposit(aliceBalance));
  
  expectBnEqual(await CC.getChannelBalance(alice), aliceBalance);
  expectBnEqual(await CC.getChannelState(alice), ChannelState.Open);
});

test('Pisa', 'Alice should be able to withdraw after Custodian confirms', setUp, async () => {
  await Alice.deposit(aliceBalance);  
  await Alice.withdrawInit();
  const events = await CC.getEvents('WithdrawInitialized');
  expect(events).to.have.length(1);
  expect(events[0].args[0].toLowerCase()).to.equal(alice);
  expectBnEqual(await CC.getChannelState(alice), ChannelState.Closing);
  const sig = await Alice.sign(CC.getAddress(), 7);
  await expectBalancesAfterTransaction(Alice, Custodian, async () => {
    const retval = await Custodian.withdrawConfirm(alice, 7, sig);
    const events = await CC.getEvents('ChannelClosed');
    expect(events).to.have.length(1);
    expect(events[0].args[0].toLowerCase()).to.equal(alice);
    return retval;
  }, 3, 7, false);
  expectBnEqual(await CC.getChannelState(alice), ChannelState.Closed);
});

test('Pisa', 'Alice should be able to withdraw everything after a timeout', setUp, async () => {
  await Alice.deposit(aliceBalance);  
  await Alice.withdrawInit();
  await shouldThrow(Alice.channelTimeout());
  await mine(delta);
  await expectBalancesAfterTransaction(Alice, Custodian, async () => {
    const retval = await Alice.channelTimeout();
    const events = await CC.getEvents('ChannelClosed');
    expect(events).to.have.length(1);
    expect(events[0].args[0].toLowerCase()).to.equal(alice);
    return retval;
  }, aliceBalance, 0, true);
  expectBnEqual(await CC.getChannelState(alice), ChannelState.Closed);
});

test('Pisa', 'Alice should be able to withdraw everything if Custodian doesn\'t ratify before timeout', setUp, async () => {
  await Alice.deposit(aliceBalance);
  const sig = await Custodian.signReceipt(unratifiedReceipt);
  await Alice.forceReceiptRatification(unratifiedReceipt, sig);
  const events = await CC.getEvents('ReceiptRatificationRequested');
  expect(events).to.have.length(1);
  expect(events[0].args[0].toLowerCase()).to.equal(alice);

  await shouldThrow(Alice.channelTimeout());
  await mine(delta);
  await expectBalancesAfterTransaction(Alice, Custodian, async () => {
    return await Alice.channelTimeout();
  }, aliceBalance, 0, true);
  expectBnEqual(await CC.getChannelState(alice), ChannelState.Closed);
});

test('Pisa', 'Custodian should be able to ratify receipt and receive payment', setUp, async () => {
  await Alice.deposit(aliceBalance);
  let sig = await Custodian.signReceipt(unratifiedReceipt);
  await Alice.forceReceiptRatification(unratifiedReceipt, sig);

  sig = await Custodian.signReceipt(ratifiedReceipt);
  await shouldThrow(Custodian.ratifyReceipt(alice, ratificationNonce - 1, sig));
  await expectBalanceAfterTransaction(Custodian, async () => {
    const retval =  await Custodian.ratifyReceipt(alice, ratificationNonce, sig);
    const events = await CC.getEvents('ReceiptRatified');
    expect(events).to.have.length(1);
    expect(events[0].args[0].toLowerCase()).to.equal(alice);
    return retval;
  }, 1);

  expectBnEqual(await CC.getChannelState(alice), ChannelState.Open);
  expectBnEqual(await CC.getChannelBalance(alice), aliceBalance - 1);
});

test('Pisa', 'ratification should automatically close the channel', setUp, async () => {
  await Alice.deposit(1);
  let sig = await Custodian.signReceipt(unratifiedReceipt);
  await Alice.forceReceiptRatification(unratifiedReceipt, sig);
  sig = await Custodian.signReceipt(ratifiedReceipt);
  await Custodian.ratifyReceipt(alice, ratificationNonce, sig);
  const events = await CC.getEvents('ChannelClosed');
  expect(events).to.have.length(1);
  expect(events[0].args[0].toLowerCase()).to.equal(alice);

  expectBnEqual(await CC.getChannelState(alice), ChannelState.Closed);
});

test('Pisa', 'custodian should be allowed to withdraw after timeout', setUp, async () => {
  await Custodian.stopMonitoring();
  await mine(tWithdraw - 1);
  await shouldThrow(Custodian.stopMonitoringTimeout());
  await mine(1);
  await Custodian.stopMonitoringTimeout();
  expect(await CC.isClosed()).to.be.true;
});
