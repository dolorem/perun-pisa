pragma solidity ^0.5.3;
pragma experimental ABIEncoderV2;

import './CommonLib.sol';
import './PerunLib.sol';
import './SafeMath.sol';

/** Implementation of Perun Ledger Channel (LC) and Virtual Channels (VCs). */
contract Perun {
  using SafeMath for int;
  using SafeMath for uint;

  /** State of a virtual channel. */
  enum VCState {
    Default,
    AliceVCStateRequested,
    AliceVCStateProvided,
    AliceSaysIngridDidntCloseVCOnTime,
    IngridProvidedVCState,
    CustodianProvidedAlreadyClosedHash,
    Closed
  }

  // LC data.
  address payable public alice;
  address payable public bob;
  uint public aliceBalance;
  uint public bobBalance;
  uint public version;  // Highest supplied version for closing.
  bytes32 public stateHash;  // Provided by the custodian.
  uint public stateHashProvidedAt;  // Block at which custodian provided state hash.
  PerunLib.State public currentState;
  bool public aliceClosing;  // True iff Alice called close() first.
  uint public blockTimeout;  // The block from which functions may be considered timed out.
  // Timeout length. Functions that initiate timeout set blockTimeout to block.number + delta.
  uint constant delta = 5;
  // Number of blocks Alice has to publish the blinding nonce if custodian attempts to resolve
  // a dispute on her behalf.
  uint constant aliceOfflineDelta = 10;
  VCDispute public lcCloseDispute;  // Dispute data.

  // Frozen balances. Changed when VC Ingrid is forced to close the channel on-chain.
  int public vcAliceSum;
  int public vcBobSum;

  struct VC {
    uint id;
    address party1;
    address party2;
    address ingrid;
    uint openingBalance1;
    uint openingBalance2;
    uint balance1;
    uint balance2;
    uint validity;  // Block number from which VC is considered no longer valid.
    VCState state;
    uint blockTimeout;  // Block number from which functions may be considered timed out.
    uint version;
  }

  /** Information provided by the custodian. */
  struct VCCustodianData {
    // Had to be separated from VC because VC struct reached the maximum number of fields.
    bytes32 stateHash;
    uint stateHashVersion;
    uint deadlineForAliceToRevealBlindingNonce;
  }

  /** Dispute data. */
  struct VCDispute {
    uint startTime;
    // All hashes provided by all custodians. Protects the custodian if Alice appointed somebody
    // else as well.
    uint[] providedByCustodian;
    bool resolvedByParty;  // True iff the dispute's been resolved by contract parties on-chain.
  }

  mapping(uint => VC) public vcs;
  mapping(uint => VCCustodianData) public vcCustodianData;
  mapping(uint => VCDispute) public vcDisputes;

  event LCOpening();
  event LCOpened();
  event LCNotOpened();
  event LCClosing();
  event LCClosed();
  event LCCloseProvidedByCustodian(bytes32 hash);

  event AliceVCStateRequested(uint vcId);
  event EVCAlreadyClosed(PerunLib.ClosingCertificate cc, bytes ccA, bytes ccI, bytes lcA, bytes lcI);
  event AliceVCStateProvided(PerunLib.VCUpdate vcUpdate, bytes sigA, bytes sigB);
  event AliceSaysIngridDidntCloseVCOnTime(uint vcId);
  event IngridClosingVC(uint vcId);
  event VCAlreadyClosedProvidedByCustodian(bytes32 hash, uint vcId);
  event VCHashProvidedByCustodian(bytes32 hash, uint vcId);
  event VCClosed(uint vcId);

  /** Get VCDispute for the given vcId. */
  function getVcDispute(uint index) public view returns (VCDispute memory) {
    return vcDisputes[index];
  }

  function getLcCloseDispute() public view returns (VCDispute memory) {
    return lcCloseDispute;
  }

  /** Get number of hashes provided by custodians in response to disputes for given VC. */
  function getProvidedByCustodianLength(uint vcId) public view returns (uint) {
    return vcDisputes[vcId].providedByCustodian.length;
  }

  /** Get a specific hash at index. */
  function getProvidedByCustodianAt(uint vcId, uint index) public view returns (uint) {
    return vcDisputes[vcId].providedByCustodian[index];
  }

  /** Get the party that isn't the intermediary Ingrid. */ 
  function vcNotIngrid(address ingrid) private view returns (address) {
    if (alice == ingrid) {
      return bob;
    }
    return alice;
  }

  modifier AliceOrBob() {
    require(msg.sender == alice || msg.sender == bob);
    _;
  }

  /** Time out an LC function */
  modifier timeout() {
    require(blockTimeout != 0 && block.number > blockTimeout);
    _;
    blockTimeout = 0;
  }

  /** Initialize a new LC timeout. */
  modifier newTimeout() {
    _;
    blockTimeout = block.number.add(delta);
  }

  /** Transition LC state. */
  modifier states(PerunLib.State from, PerunLib.State to) {
    require(currentState == from);
    _;
    currentState = to;
  }

  /** Called by Alice to initialize the contract. */
  constructor(address payable bobAddress) public payable newTimeout {
    alice = msg.sender;
    bob = bobAddress;
    aliceBalance = msg.value;
    currentState = PerunLib.State.Initializing;
    emit LCOpening();
  }

  /** Called by Bob to confirm contract initialization. */
  function openConfirm() payable public states(PerunLib.State.Initializing, PerunLib.State.Open) {
    require(msg.sender == bob);
    bobBalance = msg.value;
    emit LCOpened();
  }

  /** Called by Alice when Bob doesn't confirm contract initialization. */
  function openTimeout() public timeout states(PerunLib.State.Initializing, PerunLib.State.Closed) {
    require(msg.sender == alice);
    emit LCNotOpened();
    address(alice).transfer(address(this).balance);
  }

  /** Initialize the LC closing procedure. */
  function close(PerunLib.LCUpdate memory lcUpdate, bytes memory sig) public AliceOrBob newTimeout {
    require(currentState == PerunLib.State.Open || currentState == PerunLib.State.Closing);
    require(lcUpdate.balance1.add(lcUpdate.balance2) == aliceBalance.add(bobBalance));
    require(lcUpdate.version > 0);

    if (msg.sender == alice) {
      PerunLib.verifyLCUpdate(lcUpdate, sig, bob);
    } else {
      PerunLib.verifyLCUpdate(lcUpdate, sig, alice);
    }

    if (lcUpdate.version > version) {
      aliceBalance = lcUpdate.balance1;
      bobBalance = lcUpdate.balance2;
      version = lcUpdate.version;
    }

    if (currentState == PerunLib.State.Open) {
      aliceClosing = msg.sender == alice;
      currentState = PerunLib.State.Closing;
      lcCloseDispute.startTime = block.number;
      lcCloseDispute.resolvedByParty = false;
      emit LCClosing();
    } else {  // State.Closing.
      lcCloseDispute.resolvedByParty = true;
      require(msg.sender == (aliceClosing ? bob : alice));
      close();  
    }
  } 

  /** Called by Custodian to respond to LC closing. */
  function closeByCustodian(PerunLib.CustodianAppointment memory ca, bytes memory sig, address onBehalfOf) public {
    require(ca.lcId == address(this));
    require(ca.appointmentType == CommonLib.LCStateAppointmentType());
    require(currentState == PerunLib.State.Closing);

    if (aliceClosing) {
      require(onBehalfOf == bob);
    } else {
      require(onBehalfOf == alice);
    }

    if (onBehalfOf == alice) {
      PerunLib.verifyCA(ca, sig, alice);
    } else {
      PerunLib.verifyCA(ca, sig, bob);
    }
    
    if (ca.version > version) {
      stateHash = ca.hash;
      version = ca.version;
      stateHashProvidedAt = block.number;
    }
    lcCloseDispute.providedByCustodian.push(ca.version);
    emit LCCloseProvidedByCustodian(ca.hash);
  }

  /** Called by not-Ingrid to reveal LCUpdate that hashes to custodian's response. */
  function revealLCBlindingNonce(PerunLib.LCUpdate memory lcUpdate, uint blindingNonce, bytes memory sigA, bytes memory sigB) public {
    require(currentState == PerunLib.State.Closing);
    if (aliceClosing) {
      require(msg.sender == bob);
    } else {
      require(msg.sender == alice);
    }
    require(lcUpdate.balance1.add(lcUpdate.balance2) == aliceBalance.add(bobBalance));
    require(keccak256(abi.encodePacked(lcUpdate.id, lcUpdate.balance1, lcUpdate.balance2, lcUpdate.version, blindingNonce)) == stateHash);

    PerunLib.verifyLCUpdate(lcUpdate, sigA, alice);
    PerunLib.verifyLCUpdate(lcUpdate, sigB, bob);

    if (lcUpdate.version > version) {
      aliceBalance = lcUpdate.balance1;
      bobBalance = lcUpdate.balance2;
      version = lcUpdate.version;
    }

    close();
  }

  /** Called by either party when the other doesn't respond to LC close request. */
  function closeTimeout() public AliceOrBob timeout states(PerunLib.State.Closing, PerunLib.State.Closed) {
    if (stateHashProvidedAt != 0) {
      require(block.number > stateHashProvidedAt.add(aliceOfflineDelta));
    }
    close();
  }

  /** Close the LC. */
  function close() private {
      emit LCClosed();
      currentState = PerunLib.State.Closed;
      alice.transfer(uint(int(aliceBalance).add(vcAliceSum)));
      bob.transfer(uint(int(bobBalance).add(vcBobSum)));
      alice.transfer(address(this).balance);
  }

  /** Called by Ingrid to request VC state from Alice to close the virtual channel. */
  function requestAliceVCState(PerunLib.OpeningCertificate memory oc, bytes memory sig) public {
    require(msg.sender == oc.ingrid);
    require(block.number > oc.validity);
    VC memory vc = vcs[oc.id];
    require(vc.state == VCState.Default);
    require(currentState != PerunLib.State.Closed);

    // Set before checking the signature because it reads those fields.
    vc.party1 = oc.party1; 
    vc.party2 = oc.party2;

    if (vc.party1 == alice) {
      PerunLib.verifyOC(oc, sig, vc.party2);
    } else {
      PerunLib.verifyOC(oc, sig, vc.party1);
    }

    vc.id = oc.id;
    vc.balance1 = oc.balance1;
    vc.balance2 = oc.balance2;
    vc.state = VCState.AliceVCStateRequested;
    vc.validity = oc.validity;
    vc.blockTimeout = block.number.add(delta);
    vc.ingrid = oc.ingrid;
    vcCustodianData[vc.id].deadlineForAliceToRevealBlindingNonce = block.number.add(aliceOfflineDelta);

    vcs[vc.id] = vc;

    vcDisputes[oc.id].startTime = block.number;
    vcDisputes[oc.id].resolvedByParty = false;

    emit AliceVCStateRequested(oc.id);
  }

  /** Called by Ingrid when Alice doesn't respond to VC state request. */
  function aliceDidntProvideRequestedVCState(uint vcId) public {
    require(currentState != PerunLib.State.Closed);
    VC memory vc = vcs[vcId];
    require(msg.sender == vc.ingrid);
    if (vc.state == VCState.AliceVCStateRequested) {
      require(vc.blockTimeout != 0 && block.number > vc.blockTimeout);
    } else {
     require(vc.state == VCState.CustodianProvidedAlreadyClosedHash);
    }
    if (vcCustodianData[vcId].stateHashVersion > 0) {
      require(block.number > vcCustodianData[vcId].deadlineForAliceToRevealBlindingNonce);
    }
    emit LCClosed();
    currentState = PerunLib.State.Closed;
    msg.sender.transfer(address(this).balance);
  }

  /** Called by either party when the other attempts to do something on a closed channel. */
  function VCAlreadyClosed(PerunLib.ClosingCertificate memory cc, bytes memory ccA, bytes memory ccI, bytes memory lcA, bytes memory lcI) public AliceOrBob {
    require(currentState != PerunLib.State.Closed);
    VC memory vc = vcs[cc.vcId];
    require((msg.sender == vcNotIngrid(vc.ingrid) && vc.state == VCState.AliceVCStateRequested)
            || (msg.sender == vc.ingrid && vc.state == VCState.AliceSaysIngridDidntCloseVCOnTime)
            || (msg.sender == vcNotIngrid(vc.ingrid) && vc.state == VCState.IngridProvidedVCState));
    require(cc.lcUpdate.id == address(this));

    PerunLib.verifyCC(cc, ccA, vcNotIngrid(vc.ingrid));
    PerunLib.verifyCC(cc, ccI, vc.ingrid);
    PerunLib.verifyLCUpdate(cc.lcUpdate, lcA, vcNotIngrid(vc.ingrid));
    PerunLib.verifyLCUpdate(cc.lcUpdate, lcI, vc.ingrid);
    vc.state = VCState.Closed;

    vcs[cc.vcId] = vc;
    vcDisputes[cc.vcId].resolvedByParty = true;

    emit EVCAlreadyClosed(cc, ccA, ccI, lcA, lcI);
  }

  /** Called by Custodian when the other party attempts to do something on a closed channel. */
  function VCAlreadyClosedCustodian(PerunLib.CustodianAppointment memory ca, bytes memory sig) public {
    require(ca.lcId == address(this));
    require(currentState != PerunLib.State.Closed);
    require(ca.appointmentType == CommonLib.VCClosedAppointmentType());
    require(ca.version == 0);
    VC memory vc = vcs[ca.vcId];
    require(vc.state == VCState.AliceVCStateRequested || vc.state == VCState.IngridProvidedVCState);
    require(ca.appointmentType == CommonLib.VCClosedAppointmentType());

    PerunLib.verifyCA(ca, sig, vcNotIngrid(vc.ingrid));
    
    vcCustodianData[ca.vcId].stateHash = ca.hash; 
    vcCustodianData[ca.vcId].stateHashVersion = 1;  // Arbitrary number greater than 0. See aliceDidntProvideRequestedVCState().
    vcCustodianData[ca.vcId].deadlineForAliceToRevealBlindingNonce = block.number.add(aliceOfflineDelta);
    vcs[ca.vcId].state = VCState.CustodianProvidedAlreadyClosedHash;
    vcDisputes[ca.vcId].providedByCustodian.push(0);
  
    emit VCAlreadyClosedProvidedByCustodian(ca.hash, ca.vcId);
  }

  /** Called by not-Ingrid in response to Ingrid requesting VC state to close the virtual
      channel. */
  function provideVCState(PerunLib.VCUpdate memory vcUpdate, bytes memory sigA, bytes memory sigB) public {
    require(currentState != PerunLib.State.Closed);
    VC memory vc = vcs[vcUpdate.vcId];
    require(msg.sender == vcNotIngrid(vc.ingrid));
    require(vc.state == VCState.AliceVCStateRequested);
    require(vcUpdate.balance1.add(vcUpdate.balance2) == vc.balance1.add(vc.balance2));
    require(vcUpdate.version >= 0);

    PerunLib.verifyVCUpdate(vcUpdate, sigA, vc.party1);
    PerunLib.verifyVCUpdate(vcUpdate, sigB, vc.party2);
  
    // No need to check version since it won't be worse than the current 0.
    vc.balance1 = vcUpdate.balance1;
    vc.balance2 = vcUpdate.balance2;
    vc.version = vcUpdate.version;
    vc.state = VCState.AliceVCStateProvided;
    vc.blockTimeout = 0;

    vcs[vc.id] = vc;
    vcDisputes[vc.id].resolvedByParty = true;

    emit AliceVCStateProvided(vcUpdate, sigA, sigB);
  }

  /** Called by Custodian in response to Ingrid requesting VC state to close the virtual
      channel. */
  function provideVCStateHash(PerunLib.CustodianAppointment memory ca, bytes memory sig) public {
    require(ca.lcId == address(this));
    require(currentState != PerunLib.State.Closed);
    VC memory vc = vcs[ca.vcId];
    require(vc.state == VCState.AliceVCStateRequested);
    require(ca.appointmentType == CommonLib.VCStateAppointmentType());
  
    PerunLib.verifyCA(ca, sig, vcNotIngrid(vc.ingrid));
    
    if (ca.version > vcCustodianData[ca.vcId].stateHashVersion) {
      // Strict 'greater than' enforces that stateHashVersion will be > 0 iff
      // this VCCustodianData's been updated.
      vcCustodianData[ca.vcId].stateHash = ca.hash;
      vcCustodianData[ca.vcId].stateHashVersion = ca.version;
    }

    vcDisputes[ca.vcId].providedByCustodian.push(ca.version);

    vcs[vc.id] = vc;

    emit VCHashProvidedByCustodian(ca.hash, ca.vcId);
  }

  /** Called by not-Ingrid to reveal VCUpdate that hashes to custodian's response. */
  function revealBlindingNonce(PerunLib.VCUpdate memory vcUpdate, uint blindingNonce, bytes memory sigA, bytes memory sigB) public {
    require(currentState != PerunLib.State.Closed);
    VC memory vc = vcs[vcUpdate.vcId];
    require(vcUpdate.balance1.add(vcUpdate.balance2) == vc.balance1.add(vc.balance2));
    require(vc.state == VCState.AliceVCStateRequested);
    require(msg.sender == vcNotIngrid(vc.ingrid));
    require(keccak256(abi.encodePacked(vcUpdate.lcId, vcUpdate.vcId, vcUpdate.balance1, vcUpdate.balance2, vcUpdate.version, blindingNonce)) == vcCustodianData[vcUpdate.vcId].stateHash);

    PerunLib.verifyVCUpdate(vcUpdate, sigA, vc.party1);
    PerunLib.verifyVCUpdate(vcUpdate, sigB, vc.party2);

    vc.balance1 = vcUpdate.balance1;
    vc.balance2 = vcUpdate.balance2;
    vc.version = vcUpdate.version;
    vc.state = VCState.AliceVCStateProvided;
    vc.blockTimeout = 0;

    vcs[vc.id] = vc;

    emit AliceVCStateProvided(vcUpdate, sigA, sigB);
  }

  /** Called by not-Ingrid to reveal ClosingCertificate that hashes to custodian's response. */
  function revealAlreadyClosedBlindingNonce(PerunLib.ClosingCertificate memory cc, uint blindingNonce, bytes memory ccA, bytes memory ccI, bytes memory lcA, bytes memory lcI) public {
    require(currentState != PerunLib.State.Closed);
    VC memory vc = vcs[cc.vcId];
    require(vc.state == VCState.CustodianProvidedAlreadyClosedHash);
    require(msg.sender == vcNotIngrid(vc.ingrid));
    require(keccak256(abi.encodePacked(cc.vcId, cc.lcUpdate.id, cc.lcUpdate.balance1, cc.lcUpdate.balance2, cc.lcUpdate.version, blindingNonce)) == vcCustodianData[cc.vcId].stateHash);

    PerunLib.verifyCC(cc, ccA, vcNotIngrid(vc.ingrid));
    PerunLib.verifyCC(cc, ccI, vc.ingrid);
    PerunLib.verifyLCUpdate(cc.lcUpdate, lcA, vcNotIngrid(vc.ingrid));
    PerunLib.verifyLCUpdate(cc.lcUpdate, lcI, vc.ingrid);
    vc.state = VCState.Closed;

    vcs[cc.vcId] = vc;

    emit EVCAlreadyClosed(cc, ccA, ccI, lcA, lcI);
  }

  /** Called by not-Ingrid when Ingrid doesn't close a VC after it expires. */
  function claimThatIngridDidntCloseVCOnTime(PerunLib.OpeningCertificate memory oc, bytes memory sig) public {
    require(currentState != PerunLib.State.Closed);
    VC memory vc = vcs[oc.id];
    require(msg.sender == vcNotIngrid(vc.ingrid));
    require(vc.state != VCState.Closed && vc.state != VCState.AliceSaysIngridDidntCloseVCOnTime);
    require(block.number > oc.validity.add(delta).add(aliceOfflineDelta.add(aliceOfflineDelta)));

    if (oc.party1 == alice) {
      PerunLib.verifyOC(oc, sig, oc.party2);
    } else {
      PerunLib.verifyOC(oc, sig, oc.party1);
    }

    vc.id = oc.id;
    vc.state = VCState.AliceSaysIngridDidntCloseVCOnTime;
    vc.blockTimeout = block.number.add(delta);
    vc.ingrid = oc.ingrid;

    vcs[vc.id] = vc;

    emit AliceSaysIngridDidntCloseVCOnTime(vc.id);
  }

  /** Calledy by not-Ingrid when Ingrid didn't respond to her claim that Ingrid didn't close
      VC after it expired. */
  function ingridDidntCloseVCOnTime(uint vcId) public {
    require(currentState != PerunLib.State.Closed);
    VC memory vc = vcs[vcId];
    require(msg.sender == vcNotIngrid(vc.ingrid));
    require(vc.state == VCState.AliceSaysIngridDidntCloseVCOnTime);
    require(vc.blockTimeout != 0 && block.number > vc.blockTimeout);
    emit LCClosed();
    currentState = PerunLib.State.Closed;
    msg.sender.transfer(address(this).balance);
  }

  /** Called by Ingrid to close a VC on-chain. */
  function closeVC(
    PerunLib.OpeningCertificate memory oc,
    bytes memory ocSigAlice,
    bytes memory ocSigBob,
    PerunLib.VCUpdate memory vcUpdate,
    bytes memory currentAliceSig,
    bytes memory currentBobSig
  ) public {
    require(currentState != PerunLib.State.Closed);
    VC memory vc = vcs[oc.id];
    require(msg.sender == oc.ingrid);
    require(vc.state == VCState.AliceVCStateProvided);

    require(vcUpdate.vcId == oc.id);
    require(vcUpdate.balance1.add(vcUpdate.balance2) == oc.balance1.add(oc.balance2));

    PerunLib.verifyOC(oc, ocSigAlice, oc.party1);
    PerunLib.verifyOC(oc, ocSigBob, oc.party2);
    PerunLib.verifyVCUpdate(vcUpdate, currentAliceSig, oc.party1);
    PerunLib.verifyVCUpdate(vcUpdate, currentBobSig, oc.party2);

    if (vcUpdate.version > vc.version) {
      vc.balance1 = vcUpdate.balance1;
      vc.balance2 = vcUpdate.balance2;
      vc.version = vcUpdate.version;
    }

    vc.openingBalance1 = oc.balance1;
    vc.openingBalance2 = oc.balance2;
    vc.blockTimeout = block.number.add(delta);
    vc.state = VCState.IngridProvidedVCState;

    vcs[vc.id] = vc;

    vcDisputes[oc.id].startTime = block.number;
    vcDisputes[oc.id].resolvedByParty = false;

    emit IngridClosingVC(oc.id);
  }

  /** Called by Ingrid when Alice doesn't object to Ingrid closing the VC on-chain. */
  function closeVCTimeout(uint vcId) public {
    require(currentState != PerunLib.State.Closed);
    VC memory vc = vcs[vcId];
    require(msg.sender == vc.ingrid);
    if (vc.state == VCState.IngridProvidedVCState) {
      require(block.number > vc.blockTimeout);
    } else {
      require(vc.state == VCState.CustodianProvidedAlreadyClosedHash);
      require(block.number > vcCustodianData[vcId].deadlineForAliceToRevealBlindingNonce);
    }

    vcAliceSum = vcAliceSum.add(int(vc.balance1).sub(int(vc.openingBalance1)));
    vcBobSum = vcBobSum.add(int(vc.balance2).sub(int(vc.openingBalance2)));

    vc.state = VCState.Closed;

    vcs[vc.id] = vc;

    emit VCClosed(vc.id);
  }

  /** Called by either party when the other initializes LC closing when a VC is still open. */
  function VCActive(PerunLib.OpeningCertificate memory oc, bytes memory sig) public AliceOrBob states(PerunLib.State.Closing, PerunLib.State.Closed) {
    require(currentState != PerunLib.State.Closed);
    VC memory vc = vcs[oc.id];
    require(block.number > oc.validity.add(delta).add(aliceOfflineDelta.add(aliceOfflineDelta)));

    if (aliceClosing) {
      require(msg.sender == bob);
      PerunLib.verifyOC(oc, sig, alice);
    } else {
      require(msg.sender == alice);
      PerunLib.verifyOC(oc, sig, bob);
    }

    emit LCClosed();
    currentState = PerunLib.State.Closed;
    msg.sender.transfer(address(this).balance);
  }
}
