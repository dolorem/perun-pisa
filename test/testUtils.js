import config, {p1, p2} from './config';
import chaiAsPromised from 'chai-as-promised';
import chai from 'chai';

const BN = web3.utils.BN;

chai.use(chaiAsPromised);
const expect = chai.expect;

export const mine = async (count) => {
  if (count === undefined) {
    count = 1;
  }
  let current = await web3.eth.getBlockNumber();
  const upTo = current + count;
  while (current < upTo) {
    await web3.eth.sendTransaction({from: p1, to: p2, value: 1});
    current = await web3.eth.getBlockNumber();
  }
};

export const test = async (contractName, name, setUp, method) => {
  if (config.runAllTests) {
    await newTest(contractName, name, setUp, method);
  }
};

export const newTest = async (contractName, name, setUp, method) => {
  if (method === undefined) {
    contract('Perun', async (accounts) => {
      contract('Pisa', async (accounts_) => {
        it(contractName, async () => {
          await name(accounts, accounts_);
          await setUp();
        });
      });
    });
  } else {
    contract(contractName, async (accounts) => {
      it(name, async () => {
        await setUp(accounts);
        await method();
      });
    });
  }
};

export const shouldThrow = async (promise) => {
  await expect(promise).to.be.rejectedWith('Returned error: VM Exception while processing transaction: revert');
};

export const expectBnEqual = (x, y) => {
  if (!BN.isBN(x)) {
    return expectBnEqual(y, new BN(x));
  }
  expect(x.toString()).to.equal(y.toString());
};

export const State = {
  Initializing: new BN(0),
  Open: new BN(1),
  Closing: new BN(2),
  Closed: new BN(3),
};

export const VCState = {
  Default: new BN(0),
  AliceVCStateRequested: new BN(1),
  AliceVCStateProvided: new BN(2),
  AliceSaysIngridDidntCloseVCOnTime: new BN(3),
  IngridProvidedVCState: new BN(4),
  CustodianProvidedAlreadyClosedHash: new BN(5),
  Closed: new BN(6),
};

export const CustodianState = {
  Open: new BN(0),
  Cheated: new BN(1),
  Closed: new BN(2),
};

export const AppointmentType = {
  VCState: 0,
  LCState: 1,
  VCAlreadyClosed: 2,
};

export const expectBalancesAfterTransaction = async (Alice, Bob, promise, plusAlice, plusBob, alicePays) => {
  const balance1 = await Alice.getBalance();
  const balance2 = await Bob.getBalance();

  const gasPrice = new BN(await web3.eth.getGasPrice());

  const receipt = await promise();

  const usedGas = new BN(receipt.receipt.gasUsed);
  const spentOnGas = gasPrice.mul(usedGas);

  const balance3 = await Alice.getBalance();
  const balance4 = await Bob.getBalance();

  if (alicePays) {
    expectBnEqual(balance1.sub(spentOnGas).add(new BN(plusAlice)), balance3);
    expectBnEqual(balance2.add(new BN(plusBob)), balance4);
  } else {
    expectBnEqual(balance1.add(new BN(plusAlice)), balance3);
    expectBnEqual(balance2.sub(spentOnGas).add(new BN(plusBob)), balance4);
  }
};

export const expectBalanceAfterTransaction = async (Alice, promise, plus) => {
  const balanceBefore = await Alice.getBalance();
  const gasPrice = new BN(await web3.eth.getGasPrice());
  const receipt = await promise();
  const usedGas = new BN(receipt.receipt.gasUsed);
  const spentOnGas = gasPrice.mul(usedGas);
  const balanceAfter = await Alice.getBalance();
  expectBnEqual(balanceBefore.sub(spentOnGas).add(new BN(plus)), balanceAfter);
};

export const verifyEVCAlreadyClosed = async (CAlice, vcId, lcUpdate, ccAlice, ccIngridAlice, lcAlice, lcIngridAlice) => {
  const events = await CAlice.getEvents('EVCAlreadyClosed');
  expect(events).to.have.length(1);
  const e = events[0];
  expectBnEqual(vcId, e.args.cc.vcId);
  expect(e.args.cc.lcUpdate.id).to.equal(lcUpdate.id + '');
  expect(e.args.cc.lcUpdate.balance1).to.equal(lcUpdate.balance1 + '');
  expect(e.args.cc.lcUpdate.balance2).to.equal(lcUpdate.balance2 + '');
  expect(e.args.cc.lcUpdate.version).to.equal(lcUpdate.version + '');
  expect(e.args.ccA).to.equal(ccAlice);
  expect(e.args.ccI).to.equal(ccIngridAlice);
  expect(e.args.lcA).to.equal(lcAlice);
  expect(e.args.lcI).to.equal(lcIngridAlice);
};
