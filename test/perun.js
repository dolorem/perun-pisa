import config, {alice, bob, ingrid} from './config';
import Party from './party';
import Contract from './contract';
import {
  mine,
  test,
  newTest,
  shouldThrow,
  expectBnEqual,
  State,
  expectBalancesAfterTransaction,
} from './testUtils';
import chai from 'chai';

const expect = chai.expect;
const Perun = artifacts.require('Perun');

let Alice;
let Bob;
let Ingrid;
let C;
const aliceBalance = 10;
const bobBalance = 20;
const newAliceBalance = 5;
const newBobBalance = 25;
const newVersion = 3;
const delta = 5;
const lcUpdate = {
  id: 'filled by setUp',
  balance1: newAliceBalance,
  balance2: newBobBalance,
  version: newVersion,
};
let lcAlice;
let lcBob;

export const setUp = async (accounts) => {
  const instance =
    await Perun.new(bob, {from: alice, value: config.aliceBalance});
  const instance2 =
    await Perun.new(bob, {from: alice, value: config.aliceBalance});
  Alice =
    new Party(alice, config.aliceBalance, config.parties[0].key, instance);
  Bob = new Party(bob, config.bobBalance, config.parties[1].key, instance);
  Ingrid = new Party(ingrid, config.ingridBalance, config.parties[2].key, instance);
  C = new Contract(instance);
  lcUpdate.id = instance.address;
  lcAlice = await Alice.signLCUpdate(lcUpdate);
  lcBob = await Bob.signLCUpdate(lcUpdate);
};

test('Perun', 'constructor should emit event', setUp, async () => {
  const events = await C.getEvents('LCOpening');
  expect(events).to.have.length(1);
});

test('Perun', 'should initialize fields', setUp, async () => {
  expect(await C.getAlice()).to.equal(alice);
  expect(await C.getBob()).to.equal(bob);
  expect(State.Initializing.eq(await C.getCurrentState())).to.be.true;
  expectBnEqual(await C.getAliceBalance(), aliceBalance);
});

test('Perun', 'openTimeout should be callable only once', setUp, async () => {
  await mine(delta);
  await Alice.openTimeout();
  expect(await C.getEvents('LCNotOpened')).to.have.length(1);
  expect(await C.isClosed()).to.be.true;
});

test('Perun', 'openTimeout should be callable only by Alice', setUp, async () => {
  await shouldThrow(Bob.openTimeout());
});

test('Perun', 'openTimeout should be callable only after timeout', setUp, async () => {
  await shouldThrow(Alice.openTimeout());
});

test('Perun', 'openConfirm should be callable only by Bob', setUp, async () => {
  await shouldThrow(Alice.openConfirm());
});

test('Perun', 'openConfirm should be callable by Bob', setUp, async () => {
  await Bob.openConfirm();
  expectBnEqual(await C.getCurrentState(), State.Open);
  expectBnEqual(await C.getBobBalance(), bobBalance);

  const events = await C.getEvents('LCOpened');
  expect(events).to.have.length(1);
});

test('Perun', 'close should accept valid signature from Alice', setUp, async () => {
  await Bob.openConfirm();
  await Alice.close(lcUpdate, lcBob);
  expectBnEqual(await C.getAliceBalance(), newAliceBalance);
  expectBnEqual(await C.getBobBalance(), newBobBalance);
  expectBnEqual(await C.getCurrentState(), State.Closing);
  expect(await C.getAliceClosing()).to.be.true;
  expect(await C.getEvents('LCClosing')).to.have.length(1);
});

test('Perun', 'close should accept valid signature from Bob', setUp, async () => {
  await Bob.openConfirm();
  await Bob.close(lcUpdate, lcAlice);
  expectBnEqual(await C.getAliceBalance(), newAliceBalance);
  expectBnEqual(await C.getBobBalance(), newBobBalance);
  expectBnEqual(await C.getCurrentState(), State.Closing);
  expect(await C.getAliceClosing()).to.be.false;
});

test('Perun', 'close shouldn\'t accept parties\' signatures from themselves', setUp, async () => {
  await Bob.openConfirm();
  await shouldThrow(Alice.close(lcUpdate, lcAlice));
  await shouldThrow(Bob.close(lcUpdate, lcBob));
});

test('Perun', 'close shouldn\'t accept invalid signatures', setUp, async () => {
  await Bob.openConfirm();
  const lcInvalidUpdate = {
    id: lcUpdate.id,
    balance1: newAliceBalance + 1,
    balance2: newBobBalance - 1,
    version: newVersion,
  };

  let signature = await Alice.signLCUpdate(lcInvalidUpdate);
  await shouldThrow(Alice.close(lcInvalidUpdate, signature));

  signature = await Bob.signLCUpdate(lcInvalidUpdate);
  await shouldThrow(Bob.close(lcInvalidUpdate, signature));
});

test('Perun', 'close should maintain constant balance', setUp, async () => {
  await Bob.openConfirm();
  const lcInvalidUpdate = {
    id: lcUpdate.id,
    balance1: newAliceBalance + 2,
    balance2: newBobBalance - 1,
    version: newVersion,
  };

  let signature = await Alice.signLCUpdate(lcInvalidUpdate);
  await shouldThrow(Alice.close(lcInvalidUpdate, signature));
});

test('Perun', 'channel should be closed if everyone cooperates', setUp, async () => {
  await Bob.openConfirm();
  await Alice.close(lcUpdate, lcBob);

  await expectBalancesAfterTransaction(Alice, Bob, async () => {
    return await Bob.close(lcUpdate, lcAlice);
  }, newAliceBalance, newBobBalance, false);

  expect(await C.getEvents('LCClosed')).to.have.length(1);
  expect(await C.isClosed()).to.be.true;
});

test('Perun', 'channel should be closeable after timeout', setUp, async () => {
  await Bob.openConfirm();
  await Alice.close(lcUpdate, lcBob);

  await mine(delta);

  await expectBalancesAfterTransaction(Alice, Bob, async() => {
    return await Alice.closeTimeout();
  }, newAliceBalance, newBobBalance, true);

  expect(await C.isClosed()).to.be.true;
});

test('Perun', 'close should accept fresher signatures I', setUp, async () => {
  await Bob.openConfirm();

  await Alice.close(lcUpdate, lcBob);
  const lcUpdate2 = {
    id: lcUpdate.id,
    balance1: newAliceBalance - 1,
    balance2: newBobBalance + 1,
    version: newVersion - 1,
  };

  await expectBalancesAfterTransaction(Alice, Bob, async () => {
    let signature = await Alice.signLCUpdate(lcUpdate2);
    return await Bob.close(lcUpdate2, signature);
  }, newAliceBalance, newBobBalance, false);

  expect(await C.isClosed()).to.be.true;
});

test('Perun', 'close should accept fresher signatures II', setUp, async () => {
  await Bob.openConfirm();

  await Alice.close(lcUpdate, lcBob);
  const lcUpdate2 = {
    id: lcUpdate.id,
    balance1: newAliceBalance - 1,
    balance2: newBobBalance + 1,
    version: newVersion + 1,
  };

  await expectBalancesAfterTransaction(Alice, Bob, async () => {
    let signature = await Alice.signLCUpdate(lcUpdate2);
    return await Bob.close(lcUpdate2, signature);
  }, newAliceBalance - 1, newBobBalance + 1, false);

  expect(await C.isClosed()).to.be.true;
});
