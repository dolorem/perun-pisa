pragma solidity ^0.5.3;
pragma experimental ABIEncoderV2;

import './CommonLib.sol';

library PisaLib {
  enum ChannelState {
    Default,
    Open,
    Closing,
    Closed,
    Dispute,  // TODO(msmiech): Delete this.
    RequestedRatification
  }

  enum State {
    Open,
    Cheated,
    Closing
  }

  struct Channel {
    uint balance;
    ChannelState state;
    uint timeout;
    Receipt receiptToRatify;
  }

  struct Receipt {
    address stateChannel;
    uint virtualChannel;
    uint tStart;
    uint tEnd;
    uint nonce;
    address party;
    uint version;
    uint appointmentType;
  }

  function verifyReceipt(PisaLib.Receipt memory receipt, bytes memory sig, address expected) public pure {
    bytes32 hash = keccak256(abi.encodePacked(receipt.stateChannel, receipt.virtualChannel, receipt.tStart, receipt.tEnd, receipt.nonce, receipt.party, receipt.version, receipt.appointmentType));
    address recovered = CommonLib.recover(hash, sig);
    require(recovered == expected);
  }

  function verifyWithdrawReceipt(address contractAddress, uint value, bytes memory sig, address expected) public pure {
    bytes32 hash = keccak256(abi.encodePacked(contractAddress, value));
    address recovered = CommonLib.recover(hash, sig);
    require(recovered == expected);
  }
}
