pragma solidity ^0.5.3;
pragma experimental ABIEncoderV2;

import './CommonLib.sol';

library PerunLib {
  enum State {
    Initializing,
    Open,
    Closing,
    Closed
  }

  struct OpeningCertificate {
    uint id;
    uint balance1;
    uint balance2;
    address party1;
    address party2;
    address ingrid;
    uint validity;
    address subchannel1;
    address subchannel2;
  }

  struct LCUpdate {
    address id;
    uint balance1;
    uint balance2;
    uint version;
  }

  struct ClosingCertificate {
    uint vcId;
    LCUpdate lcUpdate;
  }

  struct VCUpdate {
    address lcId;
    uint vcId;
    uint balance1;
    uint balance2;
    uint version;
  }

  struct CustodianAppointment {
    address lcId;
    uint vcId;
    uint version;
    bytes32 hash;
    uint appointmentType;
  }

  function verifyOC(OpeningCertificate memory oc, bytes memory sig, address expected) public view {
    require(oc.subchannel1 == address(this) || oc.subchannel2 == address(this));
    bytes32 hash = keccak256(abi.encodePacked(oc.id, oc.balance1, oc.balance2, oc.party1, oc.party2, oc.ingrid, oc.validity, oc.subchannel1, oc.subchannel2));
    address recovered = CommonLib.recover(hash, sig);
    require(recovered == expected);
  }

  function verifyLCUpdate(LCUpdate memory lcUpdate, bytes memory sig, address expected) public view {
    require(lcUpdate.id == address(this));
    bytes32 hash = keccak256(abi.encodePacked(lcUpdate.id, lcUpdate.balance1, lcUpdate.balance2, lcUpdate.version));
    address recovered = CommonLib.recover(hash, sig);
    require(recovered == expected);
  }

  function verifyVCUpdate(VCUpdate memory vcUpdate, bytes memory sig, address expected) public pure {
    bytes32 hash = keccak256(abi.encodePacked(vcUpdate.lcId, vcUpdate.vcId, vcUpdate.balance1, vcUpdate.balance2, vcUpdate.version));
    address recovered = CommonLib.recover(hash, sig);
    require(recovered == expected);
  }

  function verifyCC(ClosingCertificate memory cc, bytes memory sig, address expected) public pure {
    bytes32 hash = keccak256(abi.encodePacked(cc.vcId, cc.lcUpdate.id, cc.lcUpdate.balance1, cc.lcUpdate.balance2, cc.lcUpdate.version));
    address recovered = CommonLib.recover(hash, sig);
    require(recovered == expected);
  }

  function verifyCA(CustodianAppointment memory ca, bytes memory sig, address expected) public pure {
    bytes32 hash = keccak256(abi.encodePacked(ca.lcId, ca.vcId, ca.version, ca.hash, ca.appointmentType));
    address recovered = CommonLib.recover(hash, sig);
    require(recovered == expected);
  }
}
