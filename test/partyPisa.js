const BN = web3.utils.BN;

export default class Party {
  constructor(address, key, contract) {
    this.address = address;
    this.key = key;
    this.contract = contract;
  }

  async deposit(balance) {
    return this.contract.deposit.sendTransaction({from: this.address, value: balance});
  }

  async withdrawInit(...args) {
    return this.contract.withdrawInit.sendTransaction(...args, {from: this.address});
  }

  async withdrawCheated() {
    return this.contract.withdrawCheated({from: this.address});
  }

  async withdrawConfirm(...args) {
    return this.contract.withdrawConfirm.sendTransaction(...args, {from: this.address});
  }

  async channelTimeout(...args) {
    return this.contract.channelTimeout.sendTransaction(...args, {from: this.address});
  }

  async forceReceiptRatification(...args) {
    return this.contract.forceReceiptRatification.sendTransaction(...args, {from: this.address});
  }

  async ratifyReceipt(...args) {
    return this.contract.ratifyReceipt.sendTransaction(...args, {from: this.address});
  }

  async signReceipt(receipt) {
    return await this.sign(receipt.stateChannel, receipt.virtualChannel, receipt.tStart, receipt.tEnd, receipt.nonce, receipt.party, receipt.version, receipt.appointmentType);
  }

  async sign(...args) {
    const hash = web3.utils.soliditySha3(...args);
    const signature = await web3.eth.accounts.sign(hash, this.key);
    return signature.signature;
  }

  async getBalance() {
    const balance = await web3.eth.getBalance(this.address);
    return new BN(balance);
  }

  async recourse(...args) {
    return await this.contract.recourse(...args, {from: this.address});
  }

  async stopMonitoring() {
    return await this.contract.stopMonitoring({from: this.address});
  }

  async stopMonitoringTimeout() {
    return await this.contract.stopMonitoringTimeout({from: this.address});
  }
}
