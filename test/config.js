const config = {
  parties: [
    {
      address: '0x41a1003df5445528d48624e1482883f6edd6e001',
      key: '0x5efb69c223d3f9561bb62ba53e0880df1b178b5ab7f41530be425474bac1faef',
    },
    {
      address: '0x1fd1082392eb4486a827337211b6042a897a2aa8',
      key: '0x5234cb53289a02498bea4fa67dce6c9ce344533313f0f6ac07bf71b7b2e7439c',
    },
    {
      address: '0xb4a57ed4a0e2943cc60d826e3f19fcefbd6c2bce',
      key: '0x36c5551051577434af45ed3a7eac397b010d4816ee1cfeedaf282ba10028d312',
    },
    {
      address: '0xfd97345423af8c68e1d51f9f604443d2f4abe385',
      key: '0x77142eb4fce6f20c2b6c22e398f379cafcab47e50c54830423f927fbb15bff95',
    },
    {
      address: '0x7692b7b6318749046c581ab2af04d0a94e7bae66',
      key: '0x9408bee4fc0f50824f8f4d6203118f60df8c2232c13f2f88112d98fef0e1e34a',
    },
    {
      address: '0x0b9bc811f0a56490a8e1e07035b722cbdcb21673',
      key: '0x3a52ba17b61a7a95d8f3301642b2d4318bad049b3d7fab7cb9ac2ce7ab9df424',
    },
  ],
  aliceBalance: 10,
  bobBalance: 20,
  ingridAliceBalance: 5,
  ingridBobBalance: 15,
  runAllTests: !false,
};

export const alice = config.parties[0].address;
export const bob = config.parties[1].address;
export const ingrid = config.parties[2].address;
export const custodian = config.parties[3].address;
export const p1 = config.parties[4].address;
export const p2 = config.parties[5].address;

export default config;
