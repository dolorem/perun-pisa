const Pisa = artifacts.require('Pisa');
const Perun = artifacts.require('Perun');
const PerunLib = artifacts.require('PerunLib');
const PisaLib = artifacts.require('PisaLib');
const CommonLib = artifacts.require('CommonLib');

module.exports = async function(deployer) {
  await deployer.deploy(CommonLib);
  await deployer.link(CommonLib, PerunLib);
  await deployer.link(CommonLib, PisaLib);
  await deployer.deploy(PerunLib);
  await deployer.deploy(PisaLib);
  await deployer.link(PerunLib, Perun);
  await deployer.link(CommonLib, Perun);
  await deployer.link(PisaLib, Pisa);
  await deployer.link(CommonLib, Pisa);
  await deployer.deploy(Perun, '0x1fd1082392eb4486a827337211b6042a897a2aa8', {from: '0x41a1003df5445528d48624e1482883f6edd6e001', value: 10});
  await deployer.deploy(Pisa, 5, 5, {from: '0xfd97345423af8c68e1d51f9f604443d2f4abe385', value: 1000});
};
