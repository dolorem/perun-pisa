pragma solidity ^0.5.3;
pragma experimental ABIEncoderV2;

import './CommonLib.sol';
import './Perun.sol';
import './PisaLib.sol';
import './SafeMath.sol';

contract Pisa {
  using SafeMath for uint;

  address payable public custodian;
  uint public custodianDeposit;
  uint public tSettle;
  uint public tWithdraw;
  PisaLib.State public state;
  uint constant public channelTimeoutDelta = 5;
  uint constant public receiptRatificationPrice = 1;
  uint timeout;

  event WithdrawInitialized(address who);
  event ChannelClosed(address who);
  event ReceiptRatificationRequested(address who);
  event ReceiptRatified(address who);
  event Closing();
  event CustodianCheated();

  mapping(address => PisaLib.Channel) public channels;

  constructor(uint t_settle, uint t_withdraw) public payable {
    custodian = msg.sender;
    custodianDeposit = msg.value;
    tSettle = t_settle;
    tWithdraw = t_withdraw;
  } 

  /** Called by a client to deposit money to the contract. */
  function deposit() public payable {
    require(state == PisaLib.State.Open);
    PisaLib.Channel memory channel = channels[msg.sender];
    require(channel.state == PisaLib.ChannelState.Default || channel.state == PisaLib.ChannelState.Closed);

    channel.balance = msg.value;
    channel.state = PisaLib.ChannelState.Open;

    channels[msg.sender] = channel;
  }

  /** Called by a client when it's known that the custodian's cheated and the client wants
      to withdraw their balance. */
  function withdrawCheated() public {
    require(state == PisaLib.State.Cheated);
    PisaLib.Channel memory channel = channels[msg.sender];
    require(channel.state == PisaLib.ChannelState.Open);

    uint toTransfer = channel.balance;
    channel.balance = 0;
    channel.timeout = 0;
    channel.state = PisaLib.ChannelState.Closed;

    channels[msg.sender] = channel;

    msg.sender.transfer(toTransfer);
  }

  /** Called by a client when they want to withdraw their deposit. */
  function withdrawInit() public {
    PisaLib.Channel memory channel = channels[msg.sender];
    require(channel.state == PisaLib.ChannelState.Open);

    channel.timeout = block.number.add(channelTimeoutDelta);
    channel.state = PisaLib.ChannelState.Closing;

    channels[msg.sender] = channel;

    emit WithdrawInitialized(msg.sender);
  }

  /** Called by the custodian to confirm withdrawal amount for a client. */
  function withdrawConfirm(address payable who, uint value, bytes memory sig) public {
    require(state == PisaLib.State.Open);
    require(msg.sender == custodian);
    PisaLib.verifyWithdrawReceipt(address(this), value, sig, who);

    PisaLib.Channel memory channel = channels[who];
    require(channel.state == PisaLib.ChannelState.Closing);

    uint toTransfer = channel.balance.sub(value);
    channel.balance = 0;
    channel.timeout = 0;
    channel.state = PisaLib.ChannelState.Closed;

    channels[who] = channel;

    who.transfer(toTransfer);
    custodian.transfer(value);
    
    emit ChannelClosed(who);
  }

  /** Called by a client when the custodian doesn't respond to their request. */
  function channelTimeout() public {
    require(state == PisaLib.State.Open);
    PisaLib.Channel memory channel = channels[msg.sender];
    require(channel.state == PisaLib.ChannelState.Closing || channel.state == PisaLib.ChannelState.RequestedRatification);
    require(block.number > channel.timeout);

    uint toTransfer = channel.balance;
    channel.balance = 0;
    channel.timeout = 0;
    channel.state = PisaLib.ChannelState.Closed;

    channels[msg.sender] = channel;

    msg.sender.transfer(toTransfer);

    emit ChannelClosed(msg.sender);
  }

  /** Called by a client when the custodian doesn't complete receipt
      ratification process. */
  function forceReceiptRatification(PisaLib.Receipt memory receipt, bytes memory sig) public {
    require(state == PisaLib.State.Open);
    PisaLib.Channel memory channel = channels[msg.sender];
    require(channel.state == PisaLib.ChannelState.Open);
    require(receipt.nonce == 0);
    require(channel.balance >= receiptRatificationPrice);
    require(receipt.party == msg.sender);
    require(receipt.tEnd < block.number.add(tWithdraw));

    PisaLib.verifyReceipt(receipt, sig, custodian);
    
    channel.state = PisaLib.ChannelState.RequestedRatification;
    channel.timeout = block.number.add(channelTimeoutDelta);
    channel.receiptToRatify = receipt;
    
    channels[msg.sender] = channel;

    emit ReceiptRatificationRequested(msg.sender);
  }

  /** Called by the custodian when they want to respond to the on-chain receipt
      ratification request. */
  function ratifyReceipt(address whose, uint nonce, bytes memory sig) public {
    require(state == PisaLib.State.Open);
    require(msg.sender == custodian);
    PisaLib.Channel memory channel = channels[whose];
    require(channel.state == PisaLib.ChannelState.RequestedRatification);

    channel.receiptToRatify.nonce = nonce;
    PisaLib.verifyReceipt(channel.receiptToRatify, sig, custodian);
    channel.state = PisaLib.ChannelState.Open;
    channel.balance = channel.balance.sub(receiptRatificationPrice);
    if (channel.balance == 0) {
      channel.state = PisaLib.ChannelState.Closed;
      emit ChannelClosed(whose);
    }

    channels[whose] = channel;
    custodian.transfer(receiptRatificationPrice);
    emit ReceiptRatified(whose);
  }

  /** Called by the custodian when they want to initiate the closing process. */
  function stopMonitoring() public {
    require(msg.sender == custodian);
    require(state == PisaLib.State.Open);

    timeout = block.number.add(tWithdraw);
    state = PisaLib.State.Closing;

    emit Closing();
  }

  /** Called by the custodian when they want to finalize the closing process
      and no client raised objections. */
  function stopMonitoringTimeout() public {
    require(msg.sender == custodian);
    require(state == PisaLib.State.Closing);
    require(block.number > timeout);
    selfdestruct(custodian);
  }

  /** Called by a client to claim that the custodian didn't resolve a dispute
      on their behalf. */
  function recourse(PisaLib.Receipt memory receipt, bytes memory sig) public {
    PisaLib.verifyReceipt(receipt, sig, custodian);
    require(msg.sender == receipt.party);
    Perun perun = Perun(receipt.stateChannel); 
    Perun.VCDispute memory dispute;
    if (receipt.appointmentType == CommonLib.VCStateAppointmentType() || receipt.appointmentType == CommonLib.VCClosedAppointmentType()) {
       dispute = perun.getVcDispute(receipt.virtualChannel);
    } else {
      require(receipt.appointmentType == CommonLib.LCStateAppointmentType());
      dispute = perun.getLcCloseDispute();
    }
    require(dispute.startTime > receipt.tStart);
    require(dispute.startTime < receipt.tEnd);
    require(block.number > receipt.tEnd.add(tSettle));
    if (dispute.resolvedByParty) {
      return;
    }
    for (uint i = 0; i < dispute.providedByCustodian.length; i++) {
      uint provided = dispute.providedByCustodian[i]; 
      if (receipt.version == provided) {
        return;
      }
    }
    state = PisaLib.State.Cheated;
    emit CustodianCheated();
  }
}
