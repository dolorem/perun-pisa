export default class Contract {
  constructor(instance) {
    this.instance = instance;
  }

  async getCustodian() {
    const address = await this.instance.custodian.call();
    return address.toLowerCase();
  }

  async getCustodianDeposit() {
    return await this.instance.custodianDeposit.call();
  }

  async getTSettle() {
    return await this.instance.tSettle.call();
  }
  
  async getTWithdraw() {
    return await this.instance.tWithdraw.call();
  }

  async getState() {
    return await this.instance.state.call();
  }

  async getChannelBalance(address) {
    const channel = await this.instance.channels.call(address);
    return channel.balance;
  }

  async getChannelState(address) {
    const channel = await this.instance.channels.call(address);
    return channel.state;
  }

  getAddress() {
    return this.instance.address;
  }

  async getEvents(name) {
    return await this.instance.getPastEvents(
        name, {fromBlock: 0, toBlock: 'latest'});
  }

  async isClosed() {
    const code = await web3.eth.getCode(this.instance.address);
    return code == "0x";
  }
}
